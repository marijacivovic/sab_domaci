select T1.CONSTRAINT_NAME, T1.TABLE_SCHEMA, T1.TABLE_NAME, T1.COLUMN_NAME, T2.object_id as TABLE_ID
from (
	SELECT Tab.TABLE_SCHEMA, Tab.TABLE_NAME, Col.COLUMN_NAME, Tab.CONSTRAINT_NAME from 
		INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, 
		INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col ,
		(select NAME from dbo.sysobjects where xtype='u') AS A
	WHERE 
		Col.Constraint_Name = Tab.Constraint_Name
		AND Col.Table_Name = Tab.Table_Name
		AND Constraint_Type = 'PRIMARY KEY'
		AND Col.Table_Name = A.Name
) T1
join (
	select T2.name as TABLE_SCHEMA, T1.name as TABLE_NAME, T1.object_id
	from sys.tables T1
	join sys.schemas T2
	on T1.schema_id = T2.schema_id
) T2
on T1.TABLE_SCHEMA=T2.TABLE_SCHEMA and T1.TABLE_NAME=T2.TABLE_NAME
order by T1.TABLE_SCHEMA, T1.TABLE_NAME
