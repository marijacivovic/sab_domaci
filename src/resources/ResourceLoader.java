package resources;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import map.XMLMap;

public final class ResourceLoader {

	private ResourceLoader() {

	}

	private static HashMap<String, Icon> iconsCache = new HashMap<String, Icon>();

	public static Icon loadIcon(String iconName) {
		Icon icon = null;
		if (iconsCache.containsKey(iconName)) {
			icon = iconsCache.get(iconName);
		} else {
			icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(
					ResourceLoader.class.getResource("icons/" + iconName)));
			iconsCache.put(iconName, icon);
		}
		return icon;
	}

	public static Image loadImage(String imageName) {
		Image image = Toolkit.getDefaultToolkit().getImage(
				ResourceLoader.class.getResource("images/" + imageName));
		return image;
	}

	private static XMLMap fileAssociationMap = new XMLMap(
			"config/IconAssociation.xml");

	public static XMLMap getFileAssociationMap() {
		return fileAssociationMap;
	}

	private static XMLMap viewAssociationMap = new XMLMap(
			"config/ViewAssociation.xml");

	public static XMLMap getViewAssociationMap() {
		return viewAssociationMap;
	}

	private static XMLMap fileVisibilityMap = new XMLMap(
			"config/FileVisibility.xml");

	public static XMLMap getFileVisibilityMap() {
		return fileVisibilityMap;
	}

	public static InputStream getFileInputStream(String fileName) {
		InputStream is = ResourceLoader.class.getResourceAsStream(fileName);
		return is;
	}

	private static HashMap<String, String> queriesMap = new HashMap<String, String>();

	public static String getQuery(String queryName) {
		if (!queriesMap.containsKey(queryName)) {
			InputStream s = getFileInputStream("query/" + queryName + ".sql");
			byte[] content = new byte[2048];
			int length;
			try {
				length = s.read(content);
			} catch (IOException e) {
				e.printStackTrace();
				return "";
			}
			queriesMap.put(queryName, new String(content, 0, length));
		}
		return queriesMap.get(queryName);
	}

}
