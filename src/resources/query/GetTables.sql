select T1.object_id as TableId, T2.name as SchemaName, T1.name as TableName
from sys.tables T1
left join sys.schemas T2
on T1.schema_id = T2.schema_id
order by T2.Name + '.' + T1.name
