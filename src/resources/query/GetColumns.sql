select T1.object_Id as TableId, 
	T2.name, 
	T2.column_id, 
	T2.precision,
	T2.scale,
	T2.system_type_id, 
	T2.max_length, 
	T2.is_nullable,
	T3.name as TypeName
from sys.tables T1
join sys.columns T2
on T1.object_id = T2.object_id
join sys.types T3
on T2.system_type_id = T3.system_type_id
	and T2.user_type_id = T3.user_type_id
order by tableId, column_id
