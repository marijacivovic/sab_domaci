package main;

import gui.MainWindow;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import model.tree.Node;
import model.tree.Tree;

public class Main {

	public static void main(String[] args) {

		//if (!test())return;

		MainWindow mainWindow = null;
		try {
			mainWindow = MainWindow.getInstance();
			mainWindow.setVisible(true);
		} catch (Exception ex) {
			System.out.println(ex);
		}

	}

	public static boolean test() {
		String treeFilePath = "e:\\Veliki Set Podataka\\Indeks - sekvencijalne datoteke\\studenti.tree";
		Tree tree;

		ObjectInputStream os = null;
		try {
			os = new ObjectInputStream(new FileInputStream(treeFilePath));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return true;
		} catch (IOException e1) {
			e1.printStackTrace();
			return true;
		}

		try {
			tree = (Tree) os.readObject();
			System.out.println(((Node) (tree.getRootElement().getChildAt(0)))
					.getData().get(0));
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}
}
