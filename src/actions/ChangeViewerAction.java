package actions;

import gui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;
import view.viewers.ViewerBase;

public class ChangeViewerAction extends AbstractUIAction implements
		SelectedItemChangedEventListener<ViewerBase<?>> {

	private static final long serialVersionUID = -608729209522279531L;

	private boolean isNext;

	public ChangeViewerAction(boolean isNext) {
		super(null, isNext ? "Next Tab" : "Previous Tab", isNext ? "Next Tab"
				: "Previous Tab");
		this.isNext = isNext;

		setEnabled(false);
		
		if (isNext) {
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_TAB,
					ActionEvent.CTRL_MASK));
			putValue(MNEMONIC_KEY, KeyEvent.VK_X);
		} else {
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_TAB,
					ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
			putValue(MNEMONIC_KEY, KeyEvent.VK_V);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (isNext) {
			MainWindow.getInstance().getWorkspace().showNextTab();
		} else {
			MainWindow.getInstance().getWorkspace().showPreviousTab();
		}
	}

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<ViewerBase<?>> e) {
		if (e.getSelectedItem()!=null) {
			setEnabled(true);
		} else {
			setEnabled(false);
		}
	}

}
