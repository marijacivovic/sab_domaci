package actions;

import gui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;
import view.viewers.ViewerBase;

public class CloseAllViewersAction extends AbstractUIAction implements
		SelectedItemChangedEventListener<ViewerBase<?>> {

	private static final long serialVersionUID = 3480164129501640678L;

	private ViewerBase<?> viewer;

	public CloseAllViewersAction() {
		super(null, "Close All", "Close All");

		putValue(
				ACCELERATOR_KEY,
				KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK
						| ActionEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);

		setEnabled(false);
		viewer = null;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		MainWindow.getInstance().getWorkspace().closeAllViews();
	}

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<ViewerBase<?>> e) {
		viewer = e.getSelectedItem();
		if (viewer != null) {
			setEnabled(true);
		} else {
			setEnabled(false);
		}
	}

}
