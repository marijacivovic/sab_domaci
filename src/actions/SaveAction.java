package actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import observer.EventArgs;
import observer.Observable;
import observer.Observer;
import observer.EventArguments.PropertyChangedEventArgs;

import model.Saveable;

import resources.ResourceLoader;

public class SaveAction extends AbstractUIAction implements Observer {

	private static final long serialVersionUID = 921404807204308036L;

	private Saveable saveableItem;

	public SaveAction() {
		super(ResourceLoader.loadIcon("save.png"), "Save", "Save");

		putValue(ACCELERATOR_KEY,
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_S);

		saveableItem = null;
		updateState();
	}

	private void updateState() {
		if (saveableItem == null) {
			setEnabled(false);
		} else {
			setEnabled(saveableItem.canSave());
		}
	}

	public void setSaveable(Saveable item) {
		if (saveableItem != null) {
			saveableItem.removeObserver(this);
		}
		saveableItem = item;

		if (saveableItem != null) {
			saveableItem.addObserver(this);
		}

		updateState();
	}

	public Saveable getSaveableItem() {
		return saveableItem;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (saveableItem != null) {
			saveableItem.save();
		}
	}

	@Override
	public void notifyObserver(Observable source, EventArgs args) {
		if (args != null && args instanceof PropertyChangedEventArgs) {
			if (((PropertyChangedEventArgs) args).getPropertyName().equals(
					Saveable.CAN_SAVE)) {
				updateState();
			}
		}
	}
}
