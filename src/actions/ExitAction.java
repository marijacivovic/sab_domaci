package actions;

import gui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import resources.ResourceLoader;

public class ExitAction extends AbstractUIAction {

	private static final long serialVersionUID = 921404807204308036L;

	public ExitAction() {
		super(ResourceLoader.loadIcon("exit.png"), "Exit", "Exit");
		putValue(ACCELERATOR_KEY,
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_X);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		MainWindow.getInstance().close();
	}

}
