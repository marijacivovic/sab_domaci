package actions;

import javax.swing.AbstractAction;
import javax.swing.Icon;

public abstract class AbstractUIAction extends AbstractAction {

	private static final long serialVersionUID = -1456885651840217191L;

	public AbstractUIAction(Icon icon, String name, String shortDescription) {
		putValue(SMALL_ICON, icon);
		putValue(NAME, name);
		putValue(SHORT_DESCRIPTION, shortDescription);
	}

}
