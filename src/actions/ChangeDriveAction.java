package actions;

import gui.MainWindow;

import java.awt.event.ActionEvent;
import java.io.File;

import model.FileListModel;

public class ChangeDriveAction extends AbstractUIAction {

	private static final long serialVersionUID = -1186414227549338127L;
	
	public ChangeDriveAction() {
		super(null, "", "");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		((FileListModel) MainWindow.getInstance().getSidebar().getFileList()
				.getModel()).loadFolder(new File(e.getActionCommand()));
	}

}
