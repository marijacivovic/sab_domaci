package actions;

import gui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import resources.ResourceLoader;

public class NewFileAction extends AbstractUIAction {

	private static final long serialVersionUID = -1211530386461993013L;

	private String fileExtension;

	public NewFileAction(String fileExtension) {
		super(ResourceLoader.loadIcon("new_" + fileExtension + ".png"),
				"New Text File", "New Text File");
		this.fileExtension = fileExtension;
		putValue(ACCELERATOR_KEY,
				KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_N);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		MainWindow.getInstance().getWorkspace().createNewFile(fileExtension);
	}

}
