package actions;

import gui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;

import resources.ResourceLoader;
import util.Util;

import model.FileListModel;
import model.filesystemmodel.file_types.FileSystemNode;

public class DeleteFileAction extends AbstractUIAction implements
		SelectedItemChangedEventListener<FileSystemNode> {

	private static final long serialVersionUID = -8913983019944358712L;

	private FileSystemNode itemToDelete;

	public DeleteFileAction() {
		super(ResourceLoader.loadIcon("delete.png"), "Delete", "Delete");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("DELETE"));
		putValue(MNEMONIC_KEY, KeyEvent.VK_D);
		setItemToDelete(null);
	}

	public void setItemToDelete(FileSystemNode item) {
		itemToDelete = item;
		if (itemToDelete != null && itemToDelete.canBeDeleted()) {
			setEnabled(true);
		} else {
			setEnabled(false);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (itemToDelete != null) {
			int confirmResult = JOptionPane.showConfirmDialog(null,
					"Do you sure want to delete \""
							+ itemToDelete.getFile().getName() + "\"?", "",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

			if (confirmResult == JOptionPane.YES_OPTION) {
				if (Util.recursiveDeleteFile(itemToDelete.getFile())) {
					((FileListModel) MainWindow.getInstance().getSidebar()
							.getFileList().getModel()).refresh();
				}
			}
		}
	}

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<FileSystemNode> e) {
		setItemToDelete(e.getSelectedItem());
	}

}
