package actions;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import model.data_files.IndexDataFile;
import model.tree.Node;

public class IndexTreeSelectionChangeListener implements TreeSelectionListener {

	private IndexDataFile file;

	public IndexTreeSelectionChangeListener(IndexDataFile file) {
		this.file = file;
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		if (file == null || e == null) {
			return;
		}

		Object selectedItem = e.getPath().getLastPathComponent();

		if (selectedItem == null || !(selectedItem instanceof Node)) {
			return;
		}

		Node node = (Node) selectedItem;

		if (node.isLeaf()) {
			int blockAddress = 0;
			if (node.getData().size() > 0) {
				blockAddress = node.getData().get(0).getBlockAddress();
			}

			if (file != null) {
				try {
					file.loadBlock(blockAddress);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}

	}

}
