package actions;

import gui.DBLoginDialog;
import gui.MainWindow;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import model.db.ConnectionDetails;

import resources.ResourceLoader;

public class ConnectDatabaseAction extends AbstractUIAction {

	private static final long serialVersionUID = 7018490553980517256L;

	public ConnectDatabaseAction() {
		super(ResourceLoader.loadIcon("connect_database.png"),
				"Connect Database", "Connect Database");
		putValue(ACCELERATOR_KEY,
				KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		DBLoginDialog loginDialog = new DBLoginDialog();
		loginDialog.setVisible(true);

		ConnectionDetails connectionDetails = loginDialog
				.getConnectionDetails();

		if (connectionDetails != null) {
			MainWindow.getInstance().setCursor(
					Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			try {
				MainWindow.getInstance().getWorkspace().closeAllTables();
				MainWindow.getInstance().getSidebar().getDBTree().getDBModel()
						.loadDatabase(connectionDetails);
			} catch (SQLException ex) {
				JOptionPane.showMessageDialog(MainWindow.getInstance(),
						ex.getMessage());
			}
			MainWindow.getInstance().setCursor(
					Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}

}
