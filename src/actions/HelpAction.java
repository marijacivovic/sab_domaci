package actions;

import gui.AboutDialog;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import resources.ResourceLoader;

public class HelpAction extends AbstractUIAction {

	private static final long serialVersionUID = -715913585671261869L;

	public HelpAction() {
		super(ResourceLoader.loadIcon("question.png"), "About UI Project",
				"About UI Project");

		putValue(ACCELERATOR_KEY,
				KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		AboutDialog dialog = new AboutDialog();
		dialog.setVisible(true);
	}

}
