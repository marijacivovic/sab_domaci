package actions;

import gui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import model.FileListModel;

import resources.ResourceLoader;

public class RefreshFileListAction extends AbstractUIAction {

	private static final long serialVersionUID = 7534039692615794392L;

	public RefreshFileListAction() {
		super(ResourceLoader.loadIcon("refresh.png"), "Refresh File List",
				"Refresh File List");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("F5"));
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		((FileListModel) MainWindow.getInstance().getSidebar().getFileList()
				.getModel()).refresh();
	}

}
