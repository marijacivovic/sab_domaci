package util;

import java.io.File;

public class Util {

	private Util() {
		
	}

	public static String getFileExtension(File file) {
		if (!file.exists()) {
			return "";
		}
		if (file.isDirectory()) {
			return "";
		}
		
		String fileName = file.getName();
		if (!fileName.contains(".")) {
			return "";
		}
		return fileName.substring(fileName.lastIndexOf(".")).toUpperCase();
	}
	
	public static boolean recursiveDeleteFile(File file) {
		if (!file.exists()) {
			return true;
		}

		// Disable deleting folder other than "Project test folder" for safety reasons
		if (!file.getAbsolutePath().startsWith("E:\\ProjectTestFolder")) {
			System.out.println("Deleting failed for security reasons");
			return false;
		}

		if (file.isFile()) {
			try {
				boolean deleteResult = file.delete();
				return deleteResult;
			} catch (Exception ex) {
				return false;
			}
		} else {
			File[] files = file.listFiles();
			
			if (files == null) {
				return false;
			}
			
			for (File f : files) {
				boolean deleteResult = recursiveDeleteFile(f);
				if (!deleteResult) {
					return false;
				}
			}
			
			try {
				boolean deleteResult = file.delete();
				return deleteResult;
			} catch (Exception e) {
				return false;
			}
		}	
	}
	
}
