package map;

import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import resources.ResourceLoader;

public class XMLMap {

	private String name;
	private String defaultValue;
	private HashMap<String, String> map;

	public XMLMap(String fileName) {
		map = new HashMap<String, String>();

		InputStream is = ResourceLoader.getFileInputStream(fileName);

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			Document doc = builder.parse(is);

			Node mapNode = doc.getChildNodes().item(0);

			name = mapNode.getAttributes().getNamedItem("name").getNodeValue();
			defaultValue = mapNode.getAttributes().getNamedItem("default")
					.getNodeValue();

			for (int i = 0; i < mapNode.getChildNodes().getLength(); i++) {
				Node node = mapNode.getChildNodes().item(i);

				if (node.getNodeName().equals("record")) {
					String key = node.getAttributes().getNamedItem("key")
							.getNodeValue();
					String value = node.getAttributes().getNamedItem("value")
							.getNodeValue();

					map.put(key, value);
				}
			}
		} catch (Exception ex) {
		}

	}

	public String getName() {
		return name;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public String getValue(String key) {
		if (!map.containsKey(key)) {
			return defaultValue;
		} else {
			return map.get(key);
		}
	}

}
