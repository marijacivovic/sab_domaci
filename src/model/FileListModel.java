package model;

import java.io.File;
import java.util.ArrayList;

import javax.swing.DefaultListModel;

import model.filesystemmodel.file_types.FileSystemNode;
import model.filesystemmodel.file_types.FolderModel;
import model.filesystemmodel.file_types.ParentFolderModel;

import observer.Observable;
import observer.Observer;
import observer.EventArguments.PropertyChangedEventArgs;
import resources.ResourceLoader;
import util.Util;

public class FileListModel extends DefaultListModel<FileSystemNode> implements
		Observable {

	private static final long serialVersionUID = -5075125788148551106L;

	private File currentFolder;

	public FileListModel(File initialFolder) {
		super();
		currentFolder = null;
		loadFolder(initialFolder);
	}
	
	public File getCurrentFolder() {
		return currentFolder;
	}

	public void loadFolder(File folder) {
		if (folder != currentFolder) {
			currentFolder = folder;
			refresh();
		}
	}

	public void refresh() {
		removeAllElements();

		File[] fileList = currentFolder.listFiles();

		if (currentFolder.getParentFile() != null) {
			addElement(new ParentFolderModel(currentFolder.getParentFile()));
		}

		if (fileList != null) {
			ArrayList<File> folders = new ArrayList<File>();
			ArrayList<File> files = new ArrayList<File>();

			for (File file : fileList) {
				if (file.isDirectory()) {
					folders.add(file);
				} else {
					files.add(file);
				}
			}

			for (File f : folders) {
				addElement(new FolderModel(f));
			}

			for (File f : files) {
				if (ResourceLoader.getFileVisibilityMap()
						.getValue(Util.getFileExtension(f)).equals("True")) {
					addElement(FileSystemNode.fromFile(f));
				}
			}
		} else {
			System.err.println("Error loading file list");
		}

		notifyObservers();
	}

	ArrayList<Observer> observers = new ArrayList<Observer>();

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	private void notifyObservers() {
		PropertyChangedEventArgs eventArgs = new PropertyChangedEventArgs(
				"CurrentFolder", currentFolder);
		for (Observer o : observers) {
			o.notifyObserver(this, eventArgs);
		}
	}

}
