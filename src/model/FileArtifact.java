package model;

import java.io.File;

public class FileArtifact implements SourceArtifact {

	private File file;

	public FileArtifact(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	@Override
	public String getName() {
		return file.getName();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o instanceof FileArtifact) {
			return file.equals(((FileArtifact) o).getFile());
		}

		return false;
	}

}
