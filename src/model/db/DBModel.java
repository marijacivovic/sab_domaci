package model.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.swing.tree.DefaultTreeModel;

import resources.ResourceLoader;

public class DBModel extends DefaultTreeModel {

	private static final long serialVersionUID = 2944792538706785821L;

	private Connection connection;

	public DBModel() {
		super(new DBDatabase("Not connected"));
	}

	public Connection getConnection() {
		return connection;
	}

	private HashMap<Integer, DBTable> getTables(DBDatabase database)
			throws SQLException {
		Statement getTablesStatement = connection.createStatement();

		HashMap<Integer, DBTable> tables = new HashMap<Integer, DBTable>();

		ResultSet rsTables = getTablesStatement.executeQuery(ResourceLoader
				.getQuery("GetTables"));

		while (rsTables.next()) {
			int tableId = rsTables.getInt("TableId");
			String schemaName = rsTables.getString("SchemaName");
			String tableName = rsTables.getString("TableName");

			DBTable table = new DBTable(schemaName, tableName, database);
			database.addTable(table);

			tables.put(tableId, table);
		}

		rsTables.close();

		return tables;
	}

	private void getColumns(HashMap<Integer, DBTable> tables)
			throws SQLException {
		Statement getTablesStatement = connection.createStatement();

		ResultSet rsColumns = getTablesStatement.executeQuery(ResourceLoader
				.getQuery("GetColumns"));

		while (rsColumns.next()) {
			int tableId = rsColumns.getInt("TableId");
			String columnName = rsColumns.getString("name");
			String columnType = rsColumns.getString("TypeName");
			int precision = rsColumns.getInt("precision");
			int scale = rsColumns.getInt("scale");
			int maxLength = rsColumns.getInt("max_length");
			boolean isNullable = rsColumns.getInt("is_nullable") == 1;

			DBTable table = tables.get(tableId);
			DBColumn column = new DBColumn(columnName, columnType, precision,
					scale, maxLength, isNullable, table);
			table.addColumn(column);
		}

		rsColumns.close();
	}

	private void getPrimaryKeys(HashMap<Integer, DBTable> tables)
			throws SQLException {
		Statement getPrimaryKeysStatement = connection.createStatement();

		ResultSet rsPrimaryKeys = getPrimaryKeysStatement
				.executeQuery(ResourceLoader.getQuery("GetPrimaryKeys"));

		while (rsPrimaryKeys.next()) {
			int tableId = rsPrimaryKeys.getInt("TABLE_ID");
			String columnName = rsPrimaryKeys.getString("COLUMN_NAME");
			String pkName = rsPrimaryKeys.getString("CONSTRAINT_NAME");

			DBTable table = tables.get(tableId);
			if (table != null) {
				table.setPrimaryKey(pkName);
				DBColumn column = table.getDBColumn(columnName);
				if (column != null) {
					column.setIsPrimaryKey(true);
				}
			}
		}

		rsPrimaryKeys.close();
	}

	private void getForeignKeys(HashMap<Integer, DBTable> tables)
			throws SQLException {

		HashMap<String, DBForeignKey> foreignKeys = new HashMap<String, DBForeignKey>();

		for (DBTable table : tables.values()) {
			ResultSet r = connection.getMetaData().getExportedKeys(null,
					table.getSchemaName(), table.getNameWithoutSchema());
			while (r.next()) {
				String foreignKeyName = r.getString("FK_NAME");
				String masterTableSchema = r.getString("PKTABLE_SCHEM");
				String masterTableName = r.getString("PKTABLE_NAME");
				String masterTableColumn = r.getString("PKCOLUMN_NAME");
				String childTableSchema = r.getString("FKTABLE_SCHEM");
				String childTableName = r.getString("FKTABLE_NAME");
				String childTableColumn = r.getString("FKCOLUMN_NAME");

				DBTable masterTable = null;
				DBTable childTable = null;

				for (Integer id : tables.keySet()) {
					DBTable t = tables.get(id);
					if (t.getSchemaName().equals(masterTableSchema)
							&& t.getNameWithoutSchema().equals(masterTableName)) {
						masterTable = t;
					}
					if (t.getSchemaName().equals(childTableSchema)
							&& t.getNameWithoutSchema().equals(childTableName)) {
						childTable = t;
					}
				}

				DBForeignKey fk = foreignKeys.get(foreignKeyName);
				if (fk == null) {
					fk = new DBForeignKey(foreignKeyName, masterTable,
							childTable);
					foreignKeys.put(foreignKeyName, fk);
					masterTable.addChildForeignKey(fk);
					childTable.addMasterForeignKey(fk);
				}

				DBColumn masterColumn = masterTable
						.getDBColumn(masterTableColumn);
				DBColumn childColumn = childTable.getDBColumn(childTableColumn);

				if (childColumn != null) {
					childColumn.setIsForeignKey(true);
				}
				fk.addColumn(masterColumn.getName(), childColumn.getName());
			}
			r.close();
		}
	}

	public void loadDatabase(ConnectionDetails connDetails) throws SQLException {
		if (openConnection(connDetails)) {

			DBDatabase database = new DBDatabase(connDetails.getDatabase());

			HashMap<Integer, DBTable> tables = getTables(database);

			getColumns(tables);

			getPrimaryKeys(tables);

			getForeignKeys(tables);

			setRoot(database);
		}
	}

	private boolean openConnection(ConnectionDetails connDetails)
			throws SQLException {

		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		String url = "jdbc:jtds:sqlserver://" + connDetails.getServer()
				+ ";databaseName=" + connDetails.getDatabase();

		connection = DriverManager.getConnection(url,
				connDetails.getUsername(),
				new String(connDetails.getPassword()));

		return true;

	}

}
