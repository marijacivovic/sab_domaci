package model.db;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import resources.ResourceLoader;

public class DBColumn extends DBNode {

	private DBTable table;
	private String type;
	private int precision;
	private int scale;
	private int maxLength;
	private boolean isNullable;
	private boolean isPrimaryKey;
	private boolean isForeignKey;

	public DBColumn(String name, String type, int precision, int scale,
			int maxLength, boolean isNullable, DBTable table) {
		super(name);
		this.type = type;
		this.precision = precision;
		this.scale = scale;
		this.maxLength = maxLength;
		this.table = table;
		this.isNullable = isNullable;
		this.isPrimaryKey = false;
		this.isForeignKey = false;
	}

	@Override
	public String getTitle() {
		String details = "";
		if (isPrimaryKey) {
			details += "PK, ";
		}
		if (isForeignKey) {
			details += "FK, ";
		}
		details += type;
		if (precision > 0) {
			if (!type.equals("datetime")) {
				details += "(" + precision + "," + scale + ")";
			}
		} else {
			details += "(" + maxLength + ")";
		}
		details += ", " + (isNullable ? "null" : "not null");
		return super.getTitle() + " (" + details + ")";
	}

	public String getType() {
		return type;
	}

	public boolean getIsPrimaryKey() {
		return isPrimaryKey;
	}

	public void setIsPrimaryKey(boolean value) {
		this.isPrimaryKey = value;
	}

	public boolean getIsForeignKey() {
		return isForeignKey;
	}

	public void setIsForeignKey(boolean value) {
		this.isForeignKey = value;
	}

	@Override
	public Icon getIcon() {
		if (isPrimaryKey) {
			return ResourceLoader.loadIcon("primary_key.png");
		} else if (isForeignKey) {
			return ResourceLoader.loadIcon("foreign_key.png");
		} else {
			return ResourceLoader.loadIcon("column.png");
		}
	}

	@Override
	public ArrayList<? extends DBNode> getChildren() {
		return new ArrayList<DBNode>();
	}

	@Override
	public TreeNode getParent() {
		return table;
	}

	@Override
	public String toString() {
		return super.getTitle();
	}

}
