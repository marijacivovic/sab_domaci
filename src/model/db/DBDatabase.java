package model.db;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import resources.ResourceLoader;

public class DBDatabase extends DBNode {

	private ArrayList<DBTable> tables;

	public DBDatabase(String name) {
		super(name);
		tables = new ArrayList<>();
	}

	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon("database.png");
	}

	@Override
	public ArrayList<DBTable> getChildren() {
		return tables;
	}

	@Override
	public TreeNode getParent() {
		return null;
	}

	public void addTable(DBTable table) {
		tables.add(table);
	}

}
