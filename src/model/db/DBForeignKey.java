package model.db;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import resources.ResourceLoader;

public class DBForeignKey extends DBNode {

	private DBTable masterTable;
	private DBTable childTable;
	private ArrayList<String[]> columns;

	public DBForeignKey(String name, DBTable masterTable, DBTable childTable) {
		super(name);
		this.masterTable = masterTable;
		this.childTable = childTable;
		columns = new ArrayList<>();
	}

	public void addColumn(String masterColumn, String childColumn) {
		String[] pair = new String[] { masterColumn, childColumn };
		columns.add(pair);
	}

	public int getColumnCount() {
		return columns.size();
	}

	public DBColumn getMasterColumnAt(int index) {
		return masterTable.getDBColumn(columns.get(index)[0]);
	}

	public DBColumn getChildColumnAt(int index) {
		return childTable.getDBColumn(columns.get(index)[1]);
	}

	public DBTable getMasterTable() {
		return masterTable;
	}

	public DBTable getChildTable() {
		return childTable;
	}

	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon("foreign_key.png");
	}

	@Override
	public ArrayList<? extends DBNode> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public TreeNode getParent() {
		return null;
	}

}
