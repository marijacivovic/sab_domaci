package model.db;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import model.SourceArtifact;
import resources.ResourceLoader;

public class DBTable extends DBNode implements SourceArtifact {

	private DBDatabase database;
	private String schemaName;
	private ArrayList<DBColumn> columns;
	private ArrayList<DBForeignKey> childKeys;
	private ArrayList<DBForeignKey> masterKeys;

	private ArrayList<DBNode> children;

	public DBTable(String schemaName, String name, DBDatabase database) {
		super(name);
		this.database = database;
		this.schemaName = schemaName;
		columns = new ArrayList<>();
		masterKeys = new ArrayList<>();
		childKeys = new ArrayList<>();

		children = new ArrayList<>();
		children.add(new DBFolder("Columns", this, columns));
	}

	public String getSchemaName() {
		return schemaName;
	}

	public String getNameWithoutSchema() {
		return super.getName();
	}

	public void setPrimaryKey(String primaryKeyName) {
		if (children.size() == 1) {
			children.add(new DBFolder("Primary Key", this, new DBPrimaryKey(
					primaryKeyName)));
		}
	}

	public void addMasterForeignKey(DBForeignKey fk) {
		masterKeys.add(fk);
		if (children.size() == 2) {
			children.add(new DBFolder("Foreign Keys", this, true, masterKeys));
		}
	}

	public void addChildForeignKey(DBForeignKey fk) {
		childKeys.add(fk);
	}

	public int getChildFKCount() {
		return childKeys.size();
	}

	public DBForeignKey getChildFKAt(int index) {
		return childKeys.get(index);
	}

	@Override
	public String getTitle() {
		if (schemaName == null || schemaName.equals("")) {
			return super.getName();
		}
		return schemaName + "." + super.getName();
	}

	public DBColumn getDBColumn(String columnName) {
		for (DBColumn column : columns) {
			if (column.getName().equals(columnName)) {
				return column;
			}
		}
		return null;
	}

	public DBColumn[] getColumns() {
		DBColumn[] columns = new DBColumn[this.columns.size()];
		for (int i = 0; i < columns.length; i++) {
			columns[i] = this.columns.get(i);
		}
		return columns;
	}

	public DBColumn[] getPrimaryKeySet() {
		ArrayList<DBColumn> pkSet = new ArrayList<DBColumn>();
		for (DBColumn column : columns) {
			if (column.getIsPrimaryKey()) {
				pkSet.add(column);
			}
		}
		DBColumn[] result = new DBColumn[pkSet.size()];
		for (int i = 0; i < result.length; i++) {
			result[i] = pkSet.get(i);
		}
		return result;
	}

	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon("table.png");
	}

	@Override
	public ArrayList<DBNode> getChildren() {
		return children;
	}

	@Override
	public TreeNode getParent() {
		return database;
	}

	public void addColumn(DBColumn column) {
		columns.add(column);
	}

	public String getFullPath() {
		return database.getTitle() + "." + schemaName + "." + this.getTitle();
	}

	@Override
	public String toString() {
		return getFullPath();
	}

	@Override
	public String getName() {
		return getTitle();
	}

}
