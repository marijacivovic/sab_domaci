package model.db;

import gui.MainWindow;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;
import observer.UpdateBlockEvent;
import observer.UpdateBlockEventListener;
import model.data_files.DataFileHeader;
import model.data_files.IDataFile;

public class DBTableFile implements IDataFile {

	private DBTable table;
	private DBForeignKey foreignKey;

	private Object[][] data;
	private Object[] masterData;

	public DBTableFile(DBTable table) {
		this.table = table;
		this.foreignKey = null;
	}

	public DBTable getDBTable() {
		return table;
	}

	public DBForeignKey getForeignKey() {
		return this.foreignKey;
	}

	public void setForeignKey(DBForeignKey foreignKey) {
		this.foreignKey = foreignKey;
	}

	public Object[] getMasterData() {
		return masterData;
	}

	public void setMasterData(Object[] masterData) {
		this.masterData = masterData;
		try {
			fetchNextBlock();
		} catch (IOException e) {
		} catch (SQLException e) {
		}
	}

	public Object[] getRow(int row) {
		if (data == null) {
			return null;
		}
		if (row < 0 || row > data.length) {
			return null;
		}
		return data[row];
	}

	private Connection getConnection() {
		return MainWindow.getInstance().getSidebar().getDBTree().getDBModel()
				.getConnection();
	}

	@Override
	public DataFileHeader getFileHeader() {
		return null;
	}

	@Override
	public void readHeader() throws IOException {
	}

	private String formSelectAllQuery() {

		DBColumn[] columns = table.getColumns();

		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT\n\t");

		for (int i = 0; i < columns.length; i++) {
			queryBuilder.append(columns[i].getName());
			if (i < columns.length - 1) {
				queryBuilder.append(", ");
			}
		}

		queryBuilder.append("\nFROM\n\t");
		queryBuilder.append(table.getName());
		queryBuilder.append("\n");

		return queryBuilder.toString();
	}

	private String formInsertQuery() {

		DBColumn[] columns = table.getColumns();

		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("INSERT INTO ");
		queryBuilder.append(table.getName());
		queryBuilder.append(" (");

		for (int i = 0; i < columns.length; i++) {
			queryBuilder.append(columns[i].getName());
			if (i < columns.length - 1) {
				queryBuilder.append(", ");
			} else {
				queryBuilder.append(")\n");
			}
		}

		queryBuilder.append("VALUES (");
		for (int i = 0; i < columns.length; i++) {
			queryBuilder.append('?');
			if (i < columns.length - 1) {
				queryBuilder.append(", ");
			} else {
				queryBuilder.append(")\n");
			}
		}

		return queryBuilder.toString();
	}

	private String formUpdateQuery() {

		DBColumn[] columns = table.getColumns();

		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("UPDATE ");
		queryBuilder.append(table.getName());
		queryBuilder.append("\nSET\n");

		for (int i = 0; i < columns.length; i++) {
			queryBuilder.append("\t");
			queryBuilder.append(columns[i].getName());
			queryBuilder.append("=?");
			if (i < columns.length - 1) {
				queryBuilder.append(",");
			}
			queryBuilder.append("\n");
		}

		DBColumn[] pkSet = table.getPrimaryKeySet();

		queryBuilder.append("WHERE\n");
		for (int i = 0; i < pkSet.length; i++) {
			queryBuilder.append("\t");
			queryBuilder.append(pkSet[i].getName());
			queryBuilder.append("=?");
			if (i < pkSet.length - 1) {
				queryBuilder.append(" AND");
			}
			queryBuilder.append("\n");
		}

		return queryBuilder.toString();
	}

	private String formFilterQuery(ArrayList<String[]> params) {
		StringBuilder queryBuilder = new StringBuilder(formSelectAllQuery());

		queryBuilder.append("WHERE\n");

		for (int i = 0; i < params.size(); i++) {
			queryBuilder.append("\t");
			if (i > 0) {
				queryBuilder.append("AND ");
			}
			queryBuilder.append(params.get(i)[0]);
			queryBuilder.append(" ");
			queryBuilder.append(params.get(i)[1]);
			queryBuilder.append(" ");
			queryBuilder.append("?");
			queryBuilder.append("\n");
		}

		return queryBuilder.toString();
	}

	private String formDeleteQuery() {

		DBColumn[] pkColumns = table.getPrimaryKeySet();

		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("DELETE FROM ");
		queryBuilder.append(table.getName());
		queryBuilder.append("\nWHERE\n");

		for (int i = 0; i < pkColumns.length; i++) {
			queryBuilder.append("\t");
			if (i > 0) {
				queryBuilder.append("AND ");
			}
			queryBuilder.append(pkColumns[i].getName());
			queryBuilder.append(" = ?");
			queryBuilder.append("\n");
		}

		return queryBuilder.toString();
	}

	private int getInt(String value) throws SQLException {
		try {
			return Integer.valueOf(value);
		} catch (Exception ex) {
			throw new SQLException(value + " is not a valid INT");
		}
	}

	private short getSmallInt(String value) throws SQLException {
		try {
			return Short.valueOf(value);
		} catch (Exception ex) {
			throw new SQLException(value + " is not a valid INT");
		}
	}

	private double getDouble(String value) throws SQLException {
		try {
			return Double.valueOf(value);
		} catch (Exception ex) {
			throw new SQLException(value + " is not a valid FLOAT");
		}
	}

	private BigDecimal getBigDecimal(String value) throws SQLException {
		try {
			return new BigDecimal(value);
		} catch (Exception ex) {
			throw new SQLException(value + " is not a valid NUMERIC");
		}
	}

	private short getTinyInt(String value) throws SQLException {
		try {
			return Short.valueOf(value);
		} catch (Exception ex) {
			throw new SQLException(value + " is not a valid TINYINT");
		}
	}

	private Timestamp getTimestamp(String value) throws SQLException {
		try {
			return Timestamp.valueOf(value);
		} catch (Exception ex) {
			throw new SQLException(value + " is not a valid DATETIME");
		}
	}

	private void setParameter(PreparedStatement statement, DBColumn column,
			String value, int index) throws SQLException {
		DBDataTypes type = DBDataTypes.valueOf(column.getType().toUpperCase());
		switch (type) {
		case VARCHAR:
		case CHAR:
			statement.setString(index, value);
			return;
		case NVARCHAR:
		case NCHAR:
			statement.setNString(index, value);
			return;
		case INT:
			statement.setInt(index, getInt(value));
			return;
		case SMALLINT:
			statement.setShort(index, getSmallInt(value));
			return;
		case FLOAT:
			statement.setDouble(index, getDouble(value));
			return;
		case NUMERIC:
			statement.setBigDecimal(index, getBigDecimal(value));
			return;
		case TINYINT:
			statement.setShort(index, getTinyInt(value));
			return;
		case DATETIME:
			statement.setTimestamp(index, getTimestamp(value));
			return;
		default:
			System.out.println("Unsuported data type: " + type);
			throw new SQLException("Unsuported data type: " + type);
		}
	}

	private void loadResultSet(ResultSet rs) throws SQLException {
		int columnCount = rs.getMetaData().getColumnCount();

		ArrayList<Object[]> dataList = new ArrayList<Object[]>();

		while (rs.next()) {
			Object[] row = new Object[columnCount];
			for (int i = 0; i < columnCount; i++) {
				row[i] = rs.getObject(i + 1);
			}
			dataList.add(row);
		}
		rs.close();

		data = new Object[dataList.size()][];

		int i = 0;
		for (Object[] row : dataList) {
			data[i++] = row;
		}

		notifyBlockUpdated();
	}

	private String formChildSelectAllQuery() {
		StringBuilder queryBuilder = new StringBuilder(formSelectAllQuery());
		if (this.masterData != null) {
			queryBuilder.append("WHERE\n");
			for (int i = 0; i < foreignKey.getColumnCount(); i++) {
				queryBuilder.append("\t");
				if (i > 0) {
					queryBuilder.append("AND ");
				}
				queryBuilder.append(foreignKey.getChildColumnAt(i));
				queryBuilder.append(" = ?\n");
			}
		}
		return queryBuilder.toString();
	}

	private boolean fetchNextBlockChild() throws IOException, SQLException {
		PreparedStatement statement = getConnection().prepareStatement(
				formChildSelectAllQuery());

		if (masterData != null) {
			for (int i = 0; i < foreignKey.getColumnCount(); i++) {
				DBColumn masterColumn = foreignKey.getMasterColumnAt(i);
				DBColumn[] masterTableColumns = foreignKey.getMasterTable()
						.getColumns();

				for (int j = 0; j < masterTableColumns.length; j++) {
					if (masterTableColumns[j].getName().equals(
							masterColumn.getName())) {
						setParameter(statement, masterColumn,
								String.valueOf(masterData[j]), i + 1);
					}
				}
			}
			ResultSet rs = statement.executeQuery();
			loadResultSet(rs);
		} else {
			data = new Object[0][];
			notifyBlockUpdated();
		}

		notifySelectedItemChangedEventListener(-1);
		return true;
	}

	@Override
	public boolean fetchNextBlock() throws IOException, SQLException {
		if (foreignKey != null) {
			return fetchNextBlockChild();
		}
		Statement statement = getConnection().createStatement();
		ResultSet rs = statement.executeQuery(formSelectAllQuery());
		loadResultSet(rs);
		notifySelectedItemChangedEventListener(-1);
		return true;
	}

	@Override
	public boolean addRecord(ArrayList<String> record) throws IOException,
			SQLException {
		if (record == null || record.size() != table.getColumns().length) {
			return false;
		}

		String insertQuery = formInsertQuery();

		PreparedStatement pstmt = getConnection().prepareStatement(insertQuery);

		for (int i = 0; i < record.size(); i++) {
			String cell = record.get(i);
			setParameter(pstmt, table.getColumns()[i], cell, i + 1);
		}

		pstmt.execute();

		fetchNextBlock();

		for (int row = 0; row < data.length; row++) {
			boolean equals = true;

			for (int column = 0; equals && (column < record.size()); column++) {
				Object dataCell = data[row][column];
				String recordCell = record.get(column);

				if (data == null) {
					if (recordCell != null) {
						equals = false;
					}
				} else {
					if (recordCell == null) {
						equals = false;
					} else {
						if (!String.valueOf(dataCell).trim().equals(
								String.valueOf(recordCell).trim())) {
							equals = false;
						}
					}
				}
			}

			if (equals) {
				notifySelectedItemChangedEventListener(row);
				return true;
			}
		}

		System.out.println("Record not selected");

		notifySelectedItemChangedEventListener(-1);
		return true;
	}

	@Override
	public void flush() throws IOException {

	}

	@Override
	public boolean updateRecord(ArrayList<String> record) throws IOException {
		return true;
	}

	public boolean updateRecord(ArrayList<String> record,
			ArrayList<String> newValues) throws SQLException, IOException {

		String updateQuery = formUpdateQuery();

		PreparedStatement pstmt = getConnection().prepareStatement(updateQuery);

		for (int i = 0; i < newValues.size(); i++) {
			String cell = newValues.get(i);
			setParameter(pstmt, table.getColumns()[i], cell, i + 1);
		}

		DBColumn[] pkSet = table.getPrimaryKeySet();
		for (int i = 0; i < pkSet.length; i++) {
			String oldPKValue = "";
			for (int j = 0; j < table.getColumns().length; j++) {
				if (table.getColumns()[j].getName() == pkSet[i].getName()) {
					oldPKValue = record.get(j);
				}
			}
			setParameter(pstmt, pkSet[i], oldPKValue, newValues.size() + i + 1);
		}

		try {
			pstmt.execute();
		} catch (Exception ex) {
			throw ex;
		} finally {
			pstmt.close();
		}

		fetchNextBlock();

		System.out.println("Seeking edited record");

		for (int row = 0; row < data.length; row++) {
			boolean equals = true;

			for (int column = 0; equals && (column < record.size()); column++) {
				Object dataCell = data[row][column];
				String recordCell = newValues.get(column);

				if (data == null) {
					if (recordCell != null) {
						equals = false;
					}
				} else {
					if (recordCell == null) {
						equals = false;
					} else {
						if (!String.valueOf(dataCell).trim()
								.equals(String.valueOf(recordCell).trim())) {
							equals = false;
						}
					}
				}
			}

			if (equals) {
				System.out.println(row);
				notifySelectedItemChangedEventListener(row);
				return true;
			}
		}

		return true;
	}

	@Override
	public boolean findRecord(ArrayList<String> searchRec, int[] position) {
		return true;
	}

	public boolean filterRecords(ArrayList<String[]> params)
			throws SQLException {
		String filterQuery = formFilterQuery(params);

		PreparedStatement pstmt = getConnection().prepareStatement(filterQuery);

		for (int i = 0; i < params.size(); i++) {
			String value = params.get(i)[2];
			setParameter(pstmt, table.getDBColumn(params.get(i)[0]), value,
					i + 1);
		}

		ResultSet rs = pstmt.executeQuery();

		loadResultSet(rs);

		notifySelectedItemChangedEventListener(-1);

		return true;
	}

	@Override
	public boolean deleteRecord(ArrayList<String> searchRec)
			throws SQLException, IOException {
		if (searchRec == null || searchRec.size() != table.getColumns().length) {
			return false;
		}

		DBColumn[] columns = table.getColumns();

		String deleteQuery = formDeleteQuery();

		PreparedStatement pstmt = getConnection().prepareStatement(deleteQuery);

		int index = 1;
		for (int i = 0; i < searchRec.size(); i++) {
			if (columns[i].getIsPrimaryKey()) {
				setParameter(pstmt, columns[i], searchRec.get(i), index++);
			}
		}

		pstmt.execute();

		fetchNextBlock();

		return true;
	}

	public boolean sortRecords(ArrayList<String[]> sortParams) {
		String queryBase = "";
		if (foreignKey != null) {
			if (masterData == null) {
				data = new Object[0][];
				notifyBlockUpdated();
				notifySelectedItemChangedEventListener(-1);
				return true;
			}
			queryBase = formChildSelectAllQuery();
		} else {
			queryBase = formSelectAllQuery();
		}
		try {
			StringBuilder queryBuilder = new StringBuilder(queryBase);

			queryBuilder.append("\nORDER BY\n");
			for (int i = 0; i < sortParams.size(); i++) {
				queryBuilder.append("\t" + sortParams.get(i)[0]);
				if (sortParams.get(i)[1].equals("Ascending")) {
					queryBuilder.append(" ASC");
				} else {
					queryBuilder.append(" DESC");
				}
				if (i < sortParams.size() - 1) {
					queryBuilder.append(",");
				}
				queryBuilder.append("\n");
			}

			PreparedStatement statement = getConnection().prepareStatement(
					queryBuilder.toString());

			if (foreignKey != null) {
				for (int i = 0; i < foreignKey.getColumnCount(); i++) {
					DBColumn masterColumn = foreignKey.getMasterColumnAt(i);
					DBColumn[] masterTableColumns = foreignKey.getMasterTable()
							.getColumns();

					for (int j = 0; j < masterTableColumns.length; j++) {
						if (masterTableColumns[j].getName().equals(
								masterColumn.getName())) {
							setParameter(statement, masterColumn,
									String.valueOf(masterData[j]), i + 1);
						}
					}
				}
			}

			ResultSet rs = statement.executeQuery();
			loadResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private ArrayList<UpdateBlockEventListener> updateBlockEventListeners = new ArrayList<UpdateBlockEventListener>();

	@Override
	public void addUpdateBlockEventListener(UpdateBlockEventListener listener) {
		if (listener != null) {
			updateBlockEventListeners.add(listener);
		}
	}

	@Override
	public void removeUpdateBlockEventListener(UpdateBlockEventListener listener) {
		if (listener != null) {
			updateBlockEventListeners.remove(listener);
		}
	}

	private void notifyBlockUpdated() {
		for (UpdateBlockEventListener listener : updateBlockEventListeners) {
			listener.blockUpdated(new UpdateBlockEvent(data));
		}
	}

	private ArrayList<SelectedItemChangedEventListener<Integer>> selectedItemChangedEventListeners = new ArrayList<>();

	public void addSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<Integer> listener) {
		if (listener != null) {
			selectedItemChangedEventListeners.add(listener);
		}
	}

	public void removeSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<Integer> listener) {
		if (listener != null) {
			selectedItemChangedEventListeners.remove(listener);
		}
	}

	private void notifySelectedItemChangedEventListener(int selectedItem) {
		SelectedItemChangedEvent<Integer> event = new SelectedItemChangedEvent<Integer>(
				selectedItem);
		for (SelectedItemChangedEventListener<Integer> listener : selectedItemChangedEventListeners) {
			listener.selectedItemChanged(this, event);
		}
	}

}
