package model.db;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

public abstract class DBNode implements TreeNode {

	private String name;

	public DBNode(String name) {
		this.name = name;
	}

	public String getTitle() {
		return name;
	}

	public String getName() {
		return name;
	}

	public abstract Icon getIcon();

	public abstract ArrayList<? extends DBNode> getChildren();

	@Override
	public abstract TreeNode getParent();

	@Override
	public Enumeration<?> children() {
		return null;
	}

	@Override
	public boolean getAllowsChildren() {
		return false;
	}

	@Override
	public TreeNode getChildAt(int index) {
		return getChildren().get(index);
	}

	@Override
	public int getChildCount() {
		return getChildren().size();
	}

	@Override
	public int getIndex(TreeNode e) {
		return getChildren().indexOf(e);
	}

	@Override
	public boolean isLeaf() {
		return getChildCount() == 0;
	}

}
