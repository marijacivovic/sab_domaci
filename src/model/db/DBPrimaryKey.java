package model.db;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import resources.ResourceLoader;

public class DBPrimaryKey extends DBNode {

	public DBPrimaryKey(String name) {
		super(name);
	}
	
	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon("primary_key.png");
	}

	@Override
	public ArrayList<? extends DBNode> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public TreeNode getParent() {
		return null;
	}

}
