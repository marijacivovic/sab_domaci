package model.db;

public class ConnectionDetails {

	private String server;
	private String database;
	private String username;
	private char[] password;

	public ConnectionDetails(String server, String database, String username,
			char[] password) {
		this.server = server;
		this.database = database;
		this.username = username;
		this.password = password;
	}

	public String getServer() {
		return server;
	}

	public String getDatabase() {
		return database;
	}

	public String getUsername() {
		return username;
	}

	public char[] getPassword() {
		return password.clone();
	}

}
