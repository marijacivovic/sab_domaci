package model.db;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import resources.ResourceLoader;

public class DBFolder extends DBNode {

	private DBNode parent;
	private ArrayList<DBColumn> columns;
	private DBPrimaryKey primaryKey;
	private ArrayList<DBForeignKey> foreignKeys;

	public DBFolder(String name, DBNode parent, DBPrimaryKey primaryKey) {
		super(name);
		this.parent = parent;
		this.primaryKey = primaryKey;
	}

	public DBFolder(String name, DBNode parent, ArrayList<DBColumn> columns) {
		super(name);
		this.parent = parent;
		this.columns = columns;
	}

	public DBFolder(String name, DBNode parent, boolean isFKFlder,
			ArrayList<DBForeignKey> foreignKeys) {
		super(name);
		this.parent = parent;
		this.foreignKeys = foreignKeys;
	}

	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon("tdir.png");
	}

	@Override
	public ArrayList<? extends DBNode> getChildren() {
		if (columns != null) {
			return columns;
		}
		if (foreignKeys != null) {
			return foreignKeys;
		}
		ArrayList<DBPrimaryKey> primaryKey = new ArrayList<>();
		primaryKey.add(this.primaryKey);
		return primaryKey;
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

}
