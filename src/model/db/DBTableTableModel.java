package model.db;

import javax.swing.table.DefaultTableModel;

import observer.UpdateBlockEvent;
import observer.UpdateBlockEventListener;

public class DBTableTableModel extends DefaultTableModel implements
		UpdateBlockEventListener {

	private static final long serialVersionUID = 4456414290631814215L;

	private DBTableFile tableFile;

	public DBTableTableModel(DBTableFile tableFile) {
		super(tableFile.getDBTable().getColumns(), 100);
		setColumnIdentifiers(tableFile.getDBTable().getColumns());
		this.tableFile = tableFile;
	}

	public DBTableFile getDBTableFile() {
		return tableFile;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public void blockUpdated(UpdateBlockEvent e) {
		while (getRowCount() > 0) {
			removeRow(getRowCount() - 1);
		}
		if (e != null && e.getData() != null) {
			for (Object[] row : e.getData()) {
				addRow(row);
			}
		}
	}

}
