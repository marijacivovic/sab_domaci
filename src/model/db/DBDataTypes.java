package model.db;

public enum DBDataTypes {
	IIMAGE("image", false, false, false), TEXT("text", false, false, false), UNIQUEIDENTIFIER(
			"uniqueidentifier", false, false, false), DATE("date", false,
			true, false), TIME("time", false, true, false), DATETIME2(
			"datetime2", false, true, false), DATETIMEOFFSET("datetimeoffset",
			false, true, false), TINYINT("tinyint", false, true, false), SMALLINT(
			"smallint", false, true, false), INT("int", false, true, false), SMALLDATETIME(
			"smalldatetime", false, true, false), REAL("real", false, true,
			false), MONEY("money", false, true, false), DATETIME("datetime",
			false, true, false), FLOAT("float", false, true, false), SQL_VARIANT(
			"sql_variant", false, false, false), NTEXT("ntext", false, false,
			false), BIT("bit", false, true, false), DECIMAL("decimal", false,
			true, false), NUMERIC("numeric", false, true, false), SMALLMONEY(
			"smallmoney", false, false, false), BIGINT("bigint", false, true,
			false), VARBINARY("varbinary", false, false, false), VARCHAR(
			"varchar", true, false, false), BINARY("binary", false, false,
			false), CHAR("char", true, false, false), TIMESTAMP("timestamp",
			false, true, false), NVARCHAR("nvarchar", true, false, false), SYSNAME(
			"sysname", false, false, false), NCHAR("nchar", true, false, false), HIERARCHYID(
			"hierarchyid", false, false, false), GEOMETRY("geometry", false,
			false, false), GEOGRAPHY("geography", false, false, false), XML(
			"xml", false, false, false);

	private String name;
	private boolean isLikeCompareable;
	private boolean isLessCompareable;

	private DBDataTypes(String name, boolean isLikeCompareable,
			boolean isLessCompareable, boolean t) {
		this.name = name;
		this.isLikeCompareable = isLikeCompareable;
		this.isLessCompareable = isLessCompareable;
	}

	public String getName() {
		return name;
	}

	public boolean isLikeCompareable() {
		return isLikeCompareable;
	}

	public boolean isLessCompareable() {
		return isLessCompareable;
	}

}
