package model.filesystemmodel.file_types;

import java.io.File;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleEntry;

import javax.swing.Icon;

import resources.ResourceLoader;

public class ParentFolderModel extends FolderModel {

	public ParentFolderModel(File file) {
		super(file);
	}

	@Override
	public String getDisplayName() {
		return "..";
	}

	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon("up.png");
	}

	@Override
	public boolean canBeDeleted() {
		return false;
	}

	@Override
	public ArrayList<SimpleEntry<String, String>> getAttributes() {
		return new ArrayList<SimpleEntry<String, String>>();
	}

}
