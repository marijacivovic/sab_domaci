package model.filesystemmodel.file_types;

import java.io.File;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleEntry;

import javax.swing.Icon;


import resources.ResourceLoader;

public class FolderModel extends FileSystemNode {

	public FolderModel(File file) {
		super(file);
	}

	@Override
	public String getDisplayName() {
		return file.getName();
	}
	
	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon("tdir.png");
	}
	
	@Override
	public boolean canBeDeleted() {
		return true;
	}

	@Override
	public ArrayList<SimpleEntry<String, String>> getAttributes() {
		return super.getAttributes();
	}
	
}
