package model.filesystemmodel.file_types;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleEntry;

import javax.swing.Icon;


import resources.ResourceLoader;
import util.Util;

public class FileModel extends FileSystemNode {

	public FileModel(File file) {
		super(file);
	}

	@Override
	public String getDisplayName() {
		return file.getName();
	}

	@Override
	public Icon getIcon() {
		return ResourceLoader.loadIcon(ResourceLoader.getFileAssociationMap()
				.getValue(Util.getFileExtension(file)));
	}

	@Override
	public ArrayList<SimpleEntry<String, String>> getAttributes() {
		ArrayList<SimpleEntry<String, String>> attributes = super.getAttributes();
		attributes.add(new SimpleEntry<String, String>("Size", getSizeAsString(file.length())));
		return attributes;
	}

	private String getSizeAsString(long fileSize) {
		String[] measures = new String[] { "bytes", "KB", "MB", "GB", "TB",
				"PB" };

		double size = fileSize;
		int i = 0;

		while (i < measures.length && size >= 1024.00) {
			i++;
			size /= 1024.00;
		}

		return (new DecimalFormat("#.##")).format(size) + " " + measures[i];
	}
}
