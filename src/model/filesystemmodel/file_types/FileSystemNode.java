package model.filesystemmodel.file_types;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.Icon;


public abstract class FileSystemNode {

	protected File file;

	public FileSystemNode(File file) {
		this.file = file;
	}

	public static FileSystemNode fromFile(File f) {
		if (f.isDirectory()) {
			return new FolderModel(f);
		} else {
			return new FileModel(f);
		}
	}
	
	public boolean canBeDeleted() {
		return true;
	}

	public File getFile() {
		return file;
	}

	public abstract String getDisplayName();

	public abstract Icon getIcon();
	
	public ArrayList<SimpleEntry<String, String>> getAttributes() {
		ArrayList<SimpleEntry<String, String>> attributes = new ArrayList<SimpleEntry<String, String>>();
		attributes.add(new SimpleEntry<String, String>("Name", file.getName()));
		
		Date lastModified = new Date(file.lastModified());
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd. MM. yyyy. hh:mm:ss");
		
		attributes.add(new SimpleEntry<String, String>("Last modified", dateFormat.format(lastModified)));
		return attributes;
	}

}
