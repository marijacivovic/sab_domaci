package model;

import observer.Observable;

public interface Saveable extends Observable {

	public final String CAN_SAVE = "CanSave";
	
	boolean canSave();
	
	boolean save();
	
}
