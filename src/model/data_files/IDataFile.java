package model.data_files;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import observer.UpdateBlockEventListener;

public interface IDataFile {
	
	public DataFileHeader getFileHeader();
	
	public void readHeader() throws IOException;

	public boolean fetchNextBlock() throws IOException, SQLException;

	public boolean addRecord(ArrayList<String> record) throws IOException, SQLException;
	
	public void flush() throws IOException;

	public boolean updateRecord(ArrayList<String> record) throws IOException;

	public boolean findRecord(ArrayList<String> searchRec, int[] position);

	public boolean deleteRecord(ArrayList<String> searchRec) throws SQLException, IOException;
	
	public void addUpdateBlockEventListener(UpdateBlockEventListener listener);
	
	public void removeUpdateBlockEventListener(UpdateBlockEventListener listener);
	
	
}
