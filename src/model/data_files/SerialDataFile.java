package model.data_files;

import gui.MainWindow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;
import observer.UpdateBlockEvent;
import observer.EventArguments.PropertyChangedEventArgs;

public class SerialDataFile extends DataFileBase {

	private long searchAccesses = 0;

	public SerialDataFile(File headerFile) {
		super(headerFile);
	}

	private void setSearchAccesses(long searchAccesses) {
		if (this.searchAccesses != searchAccesses) {
			this.searchAccesses = searchAccesses;
			notifyObservers(new PropertyChangedEventArgs("SEARCH_ACCESSES",
					searchAccesses));
		}
	}

	public long getSearchAccesses() {
		return searchAccesses;
	}

	private void updateFileStats(RandomAccessFile file) throws IOException {
		setFILE_SIZE(file.length());

		int recordSize = header.getRecordSize();

		if (getFILE_SIZE() % recordSize == 0) {
			setRECORD_NUM(getFILE_SIZE() / recordSize);
		} else {
			setRECORD_NUM(1 + getFILE_SIZE() / recordSize);
		}

		if (getRECORD_NUM() % getBLOCK_FACTOR() == 0) {
			setBLOCK_NUM(getRECORD_NUM() / getBLOCK_FACTOR());
		} else {
			setBLOCK_NUM(1 + getRECORD_NUM() / getBLOCK_FACTOR());
		}

	}

	public boolean fetchNextBlock() throws IOException {
		RandomAccessFile afile = new RandomAccessFile(header.getSourceFile(),
				"r");

		updateFileStats(afile);

		int recordSize = header.getRecordSize();

		if (getFILE_POINTER() / recordSize + getBLOCK_FACTOR() > getRECORD_NUM())
			setBUFFER_SIZE((int) (getRECORD_NUM() - getFILE_POINTER()
					/ recordSize)
					* recordSize);
		else
			setBUFFER_SIZE((int) (recordSize * getBLOCK_FACTOR()));

		buffer = new byte[getBUFFER_SIZE()];

		afile.seek(getFILE_POINTER());
		setBufferStart(getFILE_POINTER());
		afile.read(buffer);

		createDataFromBuffer(buffer, recordSize);

		setFILE_POINTER(afile.getFilePointer());

		afile.close();

		notifyUpdateBlockEventListeners(new UpdateBlockEvent(data));

		return true;
	}

	private String[] getRecordAt(int index, String content, int recordSize) {
		if (content.charAt(index * recordSize) == '1') {
			return null;
		}

		String line = content.substring(index * recordSize, index * recordSize
				+ recordSize);

		String[] row = new String[header.getFieldCount()];

		int k = 1;
		for (int j = 0; j < header.getFieldCount(); j++) {
			String field = null;
			field = line.substring(k, k + header.getFields()[j].getSize());
			row[j] = field;
			k = k + header.getFields()[j].getSize();
		}

		return row;
	}

	private void createDataFromBuffer(byte[] buffer, int recordSize) {
		String content = new String(buffer);

		if (content.length() < buffer.length) {
			for (int x = content.length(); x < buffer.length; x++)
				content = content + " ";
		}

		int actualRecords = 0;
		for (int i = 0; i < getBUFFER_SIZE(); i += recordSize) {
			if (content.charAt(i) == '0') {
				actualRecords++;
			}
		}

		data = new String[actualRecords][];
		int currentActualRow = 0;

		for (int i = 0; i < getBUFFER_SIZE() / recordSize; i++) {
			String[] record = getRecordAt(i, content, recordSize);
			if (record != null && currentActualRow < data.length) {
				data[currentActualRow] = getRecordAt(i, content, recordSize);
				currentActualRow++;
			}
		}
	}

	private int addBufferInsertPosition;
	private byte[] addBuffer;

	public boolean addRecord(ArrayList<String> record) throws IOException {
		if (record == null || record.size() != header.getFieldCount()) {
			return false;
		}

		String insertString = "";
		insertString = insertString + "0";
		for (String field : record) {
			insertString = insertString + field;
		}
		insertString = insertString + "\r\n";

		if (addBuffer == null) {
			addBuffer = new byte[(int) getBLOCK_FACTOR()
					* header.getRecordSize()];
			addBufferInsertPosition = 0;
		}

		for (Byte b : insertString.getBytes()) {
			addBuffer[addBufferInsertPosition++] = b;
		}

		if (addBufferInsertPosition >= addBuffer.length) {
			flush();
		}

		return true;
	}

	@Override
	public void flush() throws IOException {
		if (addBuffer == null || addBufferInsertPosition == 0) {
			return;
		}

		System.out.println("flushing");

		RandomAccessFile file = new RandomAccessFile(header.getSourceFile(),
				"rw");
		file.seek(file.length());
		file.write(addBuffer, 0, addBufferInsertPosition);
		file.close();

		addBufferInsertPosition = 0;
	}

	public boolean updateRecord(ArrayList<String> record) throws IOException {
		return false;
	}

	private boolean isFindNextSearch = false;

	public void setIsFindNextSearch(boolean value) {
		isFindNextSearch = value;
	}

	private ArrayList<String> previousSearch = null;

	private int previousSearchStop = Integer.MAX_VALUE - 1;

	public boolean findRecord(ArrayList<String> searchRec, int[] position) {

		if (!isFindNextSearch) {
			setFILE_POINTER(0);
			previousSearch = searchRec;
			previousSearchStop = Integer.MAX_VALUE - 1;
		}

		boolean result = false;

		setSearchAccesses(0);
		while (getFILE_POINTER() < getFILE_SIZE() && position[0] == -1) {
			try {
				if (previousSearchStop + 1 >= data.length) {
					fetchNextBlock();
					setSearchAccesses(getSearchAccesses() + 1);
					previousSearchStop = -1;
				}
			} catch (IOException e) {
				e.printStackTrace();
				position[0] = -1;
				return false;
			}

			for (int row = previousSearchStop + 1; row < data.length; row++) {

				if (MainWindow.getInstance().getIsClosing()) {
					return false;
				}

				notifySelectedItemChangedEventListeners(new SelectedItemChangedEvent<Integer>(
						row));
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
				}

				boolean equals = true;

				for (int field = 0; field < previousSearch.size(); field++) {
					if (!previousSearch.get(field).trim().equals("")) {
						if (!data[row][field].trim().equals(
								previousSearch.get(field).trim())) {
							equals = false;
						}
					}
				}

				if (equals) {
					position[0] = row;
					previousSearchStop = row;
					return true;
				}
			}

			previousSearchStop = Integer.MAX_VALUE - 1;
		}

		previousSearchStop = Integer.MAX_VALUE - 1;

		return result;
	}

	@SuppressWarnings("resource")
	public File findAllRecords(ArrayList<String> searchRec, int[] position) {

		String folder = header.getFile().getParent() + "\\";
		String fileName = "search_result_";

		int id = 0;
		while ((new File(folder + fileName + id + ".ser").exists())) {
			id = id + 1;
		}

		String path = folder + fileName + id + ".ser";

		System.out.println("Saving to " + path);

		try {
			RandomAccessFile raf = new RandomAccessFile(path, "rw");
			raf.writeBytes("path/" + fileName + id + ".dat" + "\n");
			for (Field field : header.getFields()) {
				raf.writeBytes(field.toFileString() + "\n");
			}
			raf.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

		RandomAccessFile dataFile;
		try {
			dataFile = new RandomAccessFile(folder + fileName + id + ".dat",
					"rw");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return null;
		}

		setFILE_POINTER(0);
		setSearchAccesses(0);
		while (getFILE_POINTER() < getFILE_SIZE() && position[0] == -1) {
			try {
				fetchNextBlock();
				setSearchAccesses(getSearchAccesses() + 1);
			} catch (IOException e) {
				e.printStackTrace();
				position[0] = -1;
				try {
					dataFile.close();
				} catch (Exception ex) {
					return null;
				}
				return null;
			}

			for (int row = 0; row < data.length; row++) {

				if (MainWindow.getInstance().getIsClosing()) {
					return null;
				}

				notifySelectedItemChangedEventListeners(new SelectedItemChangedEvent<Integer>(
						row));

				boolean equals = true;

				for (int field = 0; field < searchRec.size(); field++) {
					if (!searchRec.get(field).trim().equals("")) {
						if (!data[row][field].trim().equals(
								searchRec.get(field).trim())) {
							equals = false;
						}
					}
				}

				if (equals) {
					String insertString = "";
					insertString = insertString + "0";
					for (String field : data[row]) {
						insertString = insertString + field;
					}
					insertString = insertString + "\r\n";
					try {
						System.out.println("Writing row");
						dataFile.writeBytes(insertString);
					} catch (IOException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		try {
			dataFile.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return new File(path);
	}

	public boolean findAllRecordsInMemory(ArrayList<String> searchRec,
			int[] position) {

		ArrayList<String[]> result = new ArrayList<String[]>();

		setFILE_POINTER(0);
		setSearchAccesses(0);
		while (getFILE_POINTER() < getFILE_SIZE() && position[0] == -1) {
			try {
				fetchNextBlock();
				setSearchAccesses(getSearchAccesses() + 1);
			} catch (IOException e) {
				e.printStackTrace();
				position[0] = -1;
				return false;
			}

			for (int row = 0; row < data.length; row++) {

				if (MainWindow.getInstance().getIsClosing()) {
					return false;
				}

				notifySelectedItemChangedEventListeners(new SelectedItemChangedEvent<Integer>(
						row));

				boolean equals = true;

				for (int field = 0; field < searchRec.size(); field++) {
					if (!searchRec.get(field).trim().equals("")) {
						if (!data[row][field].trim().equals(
								searchRec.get(field).trim())) {
							equals = false;
						}
					}
				}

				if (equals) {
					result.add(data[row]);
				}
			}
		}

		data = new String[result.size()][];

		for (int i = 0; i < result.size(); i++) {
			data[i] = result.get(i);
		}

		notifyUpdateBlockEventListeners(new UpdateBlockEvent(data));

		setFILE_POINTER(0);

		return true;
	}

	public boolean deleteRecord(ArrayList<String> searchRec) {
		if (searchRec == null) {
			System.out.println("SearchRec must not be equal to NULL");
			return true;
		}

		int recordSize = header.getRecordSize();

		String content = new String(buffer);

		if (content.length() < buffer.length) {
			for (int x = content.length(); x < buffer.length; x++)
				content = content + " ";
		}

		int recordNum = content.length() / recordSize;
		for (int i = 0; i < recordNum; i++) {
			String[] record = getRecordAt(i, content, recordSize);
			if (record != null) {
				boolean equal = true;
				for (int j = 0; j < header.getFieldCount(); j++) {
					if (!record[j].equals(searchRec.get(j))) {
						equal = false;
					}
				}
				if (equal) {
					long currentPointer = getFILE_POINTER();
					try {
						RandomAccessFile raf = new RandomAccessFile(
								header.getSourceFile(), "rw");
						raf.seek(getBufferStart() + i * recordSize);
						raf.writeByte(49);
						raf.close();
					} catch (Exception ex) {
						return false;
					}
					setFILE_POINTER(currentPointer);

					buffer[i * recordSize] = 49;
					createDataFromBuffer(buffer, recordSize);
					notifyUpdateBlockEventListeners(new UpdateBlockEvent(data));
					return true;
				}
			}
		}

		return false;
	}

	private ArrayList<SelectedItemChangedEventListener<Integer>> selectedItemChangedEventListeners = new ArrayList<SelectedItemChangedEventListener<Integer>>();

	public void addSelectedItemChangedListener(
			SelectedItemChangedEventListener<Integer> listener) {
		if (listener != null) {
			selectedItemChangedEventListeners.add(listener);
		}
	}

	public void removeSelectedItemChangedListener(
			SelectedItemChangedEventListener<Integer> listener) {
		selectedItemChangedEventListeners.remove(listener);
	}

	private void notifySelectedItemChangedEventListeners(
			SelectedItemChangedEvent<Integer> event) {
		for (SelectedItemChangedEventListener<Integer> listener : selectedItemChangedEventListeners) {
			listener.selectedItemChanged(this, event);
		}
	}
}
