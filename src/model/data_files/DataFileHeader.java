package model.data_files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class DataFileHeader {

	private File file;
	private File sourceFile;
	private File treeFile;
	private File overzoneFile;
	private Field[] fields;
	private int recordSize;

	public DataFileHeader(File file, int recordAdditionalBytes)
			throws Exception {
		this.file = file;

		ArrayList<String> lines = getFileLines(file);

		if (lines == null) {
			throw new Exception("Failed to load file header.");
		}

		ArrayList<Field> tempFields = new ArrayList<Field>();
		for (String line : lines) {
			String[] lineParts = line.split("/");
			if (lineParts.length > 0) {
				if (lineParts[0].equals("path")) {
					sourceFile = new File(file.getParent() + "\\"
							+ lineParts[1]);
				} else if (lineParts[0].equals("tree")) {
					treeFile = new File(file.getParent() + "\\" + lineParts[1]);
				} else if (lineParts[0].equals("overZone")) {
					overzoneFile = new File(file.getParent() + "\\"
							+ lineParts[1]);
				} else if (lineParts[0].equals("field")) {
					String fieldName = lineParts[1];
					Types fieldType = Types.valueOf(lineParts[2]);
					int fieldSize = Integer.parseInt(lineParts[3]);
					boolean fieldIsPrimary = Boolean.parseBoolean(lineParts[4]);

					tempFields.add(new Field(fieldName, fieldType, fieldSize,
							fieldIsPrimary));
				}
			}
		}

		fields = new Field[tempFields.size()];
		recordSize = 0;
		for (int i = 0; i < tempFields.size(); i++) {
			fields[i] = tempFields.get(i);
			recordSize = recordSize + fields[i].getSize();
		}
		recordSize = recordSize + recordAdditionalBytes;
	}

	private ArrayList<String> getFileLines(File file) {
		ArrayList<String> lines = new ArrayList<String>();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
			br.close();
		} catch (Exception e) {
			lines = null;
			e.printStackTrace();
		}
		return lines;
	}

	public File getFile() {
		return file;
	}

	public File getSourceFile() {
		return sourceFile;
	}

	public File getTreeFile() {
		return treeFile;
	}

	public File getOverzoneFile() {
		return overzoneFile;
	}

	public int getFieldCount() {
		if (fields == null) {
			return -1;
		}
		return fields.length;
	}

	public Field[] getFields() {
		return fields;
	}

	public int getRecordSize() {
		return recordSize;
	}

}
