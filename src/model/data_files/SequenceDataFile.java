package model.data_files;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import observer.UpdateBlockEvent;

public class SequenceDataFile extends DataFileBase {

	public SequenceDataFile(File headerFile) {
		super(headerFile);
	}

	private void updateFileStats(RandomAccessFile file) throws IOException {
		setFILE_SIZE(file.length());

		int recordSize = header.getRecordSize();

		if (getFILE_SIZE() % recordSize == 0) {
			setRECORD_NUM(getFILE_SIZE() / recordSize);
		} else {
			setRECORD_NUM(1 + getFILE_SIZE() / recordSize);
		}

		if (getRECORD_NUM() % getBLOCK_FACTOR() == 0) {
			setBLOCK_NUM(getRECORD_NUM() / getBLOCK_FACTOR());
		} else {
			setBLOCK_NUM(1 + getRECORD_NUM() / getBLOCK_FACTOR());
		}

	}

	@Override
	public boolean fetchNextBlock() throws IOException {
		RandomAccessFile afile = new RandomAccessFile(header.getSourceFile(),
				"r");

		updateFileStats(afile);

		int recordSize = header.getRecordSize();

		if (getFILE_POINTER() / recordSize + getBLOCK_FACTOR() > getRECORD_NUM())
			setBUFFER_SIZE((int) (getRECORD_NUM() - getFILE_POINTER()
					/ recordSize)
					* recordSize);
		else
			setBUFFER_SIZE((int) (recordSize * getBLOCK_FACTOR()));

		buffer = new byte[getBUFFER_SIZE()];

		afile.seek(getFILE_POINTER());
		setBufferStart(getFILE_POINTER());
		afile.read(buffer);

		try {
			createDataFromBuffer(buffer, recordSize);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		setFILE_POINTER(afile.getFilePointer());

		afile.close();

		notifyUpdateBlockEventListeners(new UpdateBlockEvent(data));

		return true;
	}

	private String[] getRecordAt(int index, String content, int recordSize) {
		String line = content.substring(index * recordSize, index * recordSize
				+ recordSize);

		String[] row = new String[header.getFieldCount()];

		int k = 0;
		for (int j = 0; j < header.getFieldCount(); j++) {
			String field = null;
			field = line.substring(k, k + header.getFields()[j].getSize());
			row[j] = field;
			k = k + header.getFields()[j].getSize();
		}

		return row;
	}

	private void createDataFromBuffer(byte[] buffer, int recordSize) {
		String content = new String(buffer);

		if (content.length() < buffer.length) {
			for (int x = content.length(); x < buffer.length; x++)
				content = content + " ";
		}

		int actualRecords = 0;
		for (int i = 0; i < getBUFFER_SIZE(); i += recordSize) {
			actualRecords++;
		}

		data = new String[actualRecords][];
		int currentActualRow = 0;

		for (int i = 0; i < getBUFFER_SIZE() / recordSize; i++) {
			String[] record = getRecordAt(i, content, recordSize);
			if (record != null) {
				data[currentActualRow++] = getRecordAt(i, content, recordSize);
			}
		}
	}

	@Override
	public boolean addRecord(ArrayList<String> record) throws IOException {
		return false;
	}

	@Override
	public void flush() throws IOException {

	}

	@Override
	public boolean updateRecord(ArrayList<String> record) throws IOException {
		int[] positions = new int[1];
		boolean findResult = binarySearch(record, positions);

		if (!findResult) {
			return true;
		}

		try {
			RandomAccessFile raf = new RandomAccessFile(header.getSourceFile(),
					"rw");

			raf.seek(positions[0] * header.getRecordSize());

			String insertString = "";
			for (String s : record) {
				insertString += s;
			}

			raf.writeBytes(insertString);

			raf.close();

			setFILE_POINTER(getBufferStart());
			fetchNextBlock();
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return true;
	}

	private String[] getRecordAt(long index) {
		int recordSize = header.getRecordSize();
		buffer = new byte[recordSize];

		try {
			RandomAccessFile afile = new RandomAccessFile(
					header.getSourceFile(), "r");
			afile.seek(index * recordSize);
			afile.read(buffer);
			afile.close();
		} catch (Exception ex) {
			return null;
		}

		String recordString = new String(buffer);

		while (recordString.length() < recordSize) {
			recordString = recordString + " ";
		}

		String[] record = new String[header.getFieldCount()];

		int position = 0;
		for (int i = 0; i < record.length; i++) {
			record[i] = recordString.substring(position,
					position + header.getFields()[i].getSize());
			position += header.getFields()[i].getSize();
		}

		return record;
	}

	private int compareRows(String[] row, ArrayList<String> searchRec) {
		for (int col = 0; col < header.getFieldCount(); col++) {
			if (header.getFields()[col].getIsPrimary()) {
				String s1 = row[col].trim().toUpperCase();
				String s2 = searchRec.get(col).trim().toUpperCase();
				int cmp = compareValues(s1, s2, header.getFields()[col]);
				if (cmp < 0) {
					return -1;
				} else if (cmp > 0) {
					return 1;
				}
			}
		}
		return 0;
	}

	private boolean binarySearch(ArrayList<String> searchRec, int[] position) {
		if (searchRec == null || searchRec.size() != header.getFieldCount()) {
			return false;
		}

		ArrayList<Integer> primaryKeyIndices = new ArrayList<Integer>();

		for (int i = 0; i < header.getFieldCount(); i++) {
			if (header.getFields()[i].getIsPrimary()) {
				primaryKeyIndices.add(i);
			}
		}

		long left = 0;
		long right = getRECORD_NUM() - 1;

		while (left <= right) {
			long m = (right + left) / 2;
			String[] mRecord = getRecordAt(m);
			if (mRecord == null) {
				return false;
			}

			int compareResult = compareRows(mRecord, searchRec);

			if (compareResult == 0) {
				position[0] = (int) m;
				return true;
			} else if (compareResult < 0) {
				left = m + 1;
			} else {
				right = m - 1;
			}

		}

		return false;
	}

	@Override
	public boolean findRecord(ArrayList<String> searchRec, int[] position) {
		boolean result = binarySearch(searchRec, position);
		if (result) {
			setFILE_POINTER(position[0] * header.getRecordSize());
			try {
				fetchNextBlock();
			} catch (Exception ex) {
			}
		}
		return result;
	}

	@Override
	public boolean deleteRecord(ArrayList<String> searchRec) {
		int[] positions = new int[1];
		boolean findResult = binarySearch(searchRec, positions);

		if (!findResult) {
			return true;
		}

		System.out.println("Position for delete = " + positions[0]);

		try {
			RandomAccessFile raf = new RandomAccessFile(header.getSourceFile(),
					"rw");

			long writePointer = positions[0] * header.getRecordSize();
			long readPointer = writePointer + header.getRecordSize();
			byte[] buffer = new byte[(int) getBLOCK_FACTOR()
					* header.getRecordSize()];
			System.out.println("Buffer size = " + buffer.length);

			while (readPointer < raf.length()) {
				raf.seek(readPointer);
				int readLength = raf.read(buffer);
				readPointer += readLength;

				raf.seek(writePointer);
				raf.write(buffer, 0, readLength);
				writePointer += readLength;
			}

			long newLength = 0;
			if (raf.length() > header.getRecordSize()) {
				newLength = raf.length() - header.getRecordSize();
			}

			raf.setLength(newLength);

			raf.close();

			setFILE_POINTER(getBufferStart());
			fetchNextBlock();
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return false;
	}

}
