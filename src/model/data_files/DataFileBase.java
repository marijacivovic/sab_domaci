package model.data_files;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import observer.EventArgs;
import observer.Observable;
import observer.Observer;
import observer.UpdateBlockEvent;
import observer.UpdateBlockEventListener;
import observer.EventArguments.PropertyChangedEventArgs;
import util.Util;

public abstract class DataFileBase implements IDataFile, Observable {
	static public final int BROWSE_MODE = 1;
	static public final int ADD_MODE = 2;
	static public final int UPDATE_MODE = 3;
	static public final int DELETE_MODE = 4;
	static public final int FIND_MODE = 5;

	private long BLOCK_FACTOR = 20;
	private long FILE_SIZE = 0;
	private long BLOCK_NUM = 0;
	private long RECORD_NUM = 0;

	private int BUFFER_SIZE = 0;
	private long FILE_POINTER = 0;

	private int MODE = DataFileBase.BROWSE_MODE;

	protected File headerFile;
	protected DataFileHeader header;

	protected long bufferStart = 0;
	protected byte[] buffer;
	protected String[][] data = null;

	public DataFileBase(File headerFile) {
		this.headerFile = headerFile;
	}

	public void readHeader() throws IOException {
		try {
			if (Util.getFileExtension(headerFile).equals(".SER")) {
				header = new DataFileHeader(headerFile, 3);
			} else if (Util.getFileExtension(headerFile).equals(".SEK")) {
				header = new DataFileHeader(headerFile, 2);
			} else if (Util.getFileExtension(headerFile).equals(".IND")) {
				header = new DataFileHeader(headerFile, 2);
			}
		} catch (Exception e) {
			System.err.println(e);
		}

		RandomAccessFile afile = new RandomAccessFile(header.getSourceFile(),
				"r");

		FILE_SIZE = afile.length();

		RECORD_NUM = (long) Math.ceil((FILE_SIZE * 1.0000)
				/ (header.getRecordSize() * 1.0000));
		BLOCK_NUM = (int) (RECORD_NUM / BLOCK_FACTOR) + 1;

		afile.close();
	}

	public DataFileHeader getFileHeader() {
		return header;
	}

	public void setBLOCK_FACTOR(long block_size) {
		BLOCK_FACTOR = block_size;
		BLOCK_NUM = (int) (RECORD_NUM / BLOCK_FACTOR) + 1;
		notifyObservers(new PropertyChangedEventArgs("BLOCK_FACTOR", block_size));
	}

	public long getBLOCK_FACTOR() {
		return BLOCK_FACTOR;
	}

	protected void setBLOCK_NUM(long blockNum) {
		if (BLOCK_NUM != blockNum) {
			BLOCK_NUM = blockNum;
			notifyObservers(new PropertyChangedEventArgs("BLOCK_NUM", blockNum));
		}
	}

	public long getBLOCK_NUM() {
		return BLOCK_NUM;
	}

	public byte[] getBlockContent() {
		return buffer;
	}

	public long getFILE_POINTER() {
		return FILE_POINTER;
	}

	protected void setFILE_POINTER(long filePointer) {
		if (FILE_POINTER != filePointer) {
			FILE_POINTER = filePointer;
			notifyObservers(new PropertyChangedEventArgs("FILE_POINTER",
					filePointer));
		}
	}

	public long getFILE_SIZE() {
		return FILE_SIZE;
	}

	protected void setFILE_SIZE(long fileSize) {
		if (FILE_SIZE != fileSize) {
			FILE_SIZE = fileSize;
			notifyObservers(new PropertyChangedEventArgs("FILE_SIZE", fileSize));
		}
	}

	protected void setRECORD_NUM(long recordNum) {
		if (RECORD_NUM != recordNum) {
			RECORD_NUM = recordNum;
			notifyObservers(new PropertyChangedEventArgs("RECORD_NUM",
					recordNum));
		}
	}

	public long getRECORD_NUM() {
		return RECORD_NUM;
	}

	public int getBUFFER_SIZE() {
		return BUFFER_SIZE;
	}

	public void setBUFFER_SIZE(int bufferSize) {
		if (BUFFER_SIZE != bufferSize) {
			BUFFER_SIZE = bufferSize;
			notifyObservers(new PropertyChangedEventArgs("BUFFER_SIZE",
					bufferSize));
		}
	}

	public String[][] getData() {
		return data;
	}

	public int getMODE() {
		return MODE;
	}

	public void setMODE(int mode) {
		if (MODE != mode) {
			MODE = mode;
			notifyObservers(new PropertyChangedEventArgs("MODE", mode));
		}
	}

	public long getBufferStart() {
		return bufferStart;
	}

	protected void setBufferStart(long bufferStart) {
		this.bufferStart = bufferStart;
	}

	private ArrayList<Observer> observers = new ArrayList<Observer>();

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	protected void notifyObservers(EventArgs args) {
		for (Observer observer : observers) {
			observer.notifyObserver(this, args);
		}
	}

	private ArrayList<UpdateBlockEventListener> listeners = new ArrayList<UpdateBlockEventListener>();

	public void addUpdateBlockEventListener(UpdateBlockEventListener listener) {
		if (listener != null) {
			listeners.add(listener);
		}
	}

	public void removeUpdateBlockEventListener(UpdateBlockEventListener listener) {
		if (listener != null) {
			listeners.remove(listener);
		}
	}

	protected void notifyUpdateBlockEventListeners(UpdateBlockEvent e) {
		for (UpdateBlockEventListener listener : listeners) {
			listener.blockUpdated(e);
		}
	}

	protected int compareValues(String s1, String s2, Field field) {
		if (field.getType() == Types.TYPE_CHAR) {
			return s1.compareTo(s2);
		} else if (field.getType() == Types.TYPE_VARCHAR) {
			if (s1.length() == s2.length()) {
				return s1.compareTo(s2);
			} else if (s1.length() < s2.length()) {
				return -1;
			} else {
				return 1;
			}
		}
		return 0;
	}

}
