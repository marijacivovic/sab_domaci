package model.data_files;

public enum Types {
	TYPE_VARCHAR	("TYPE_VARCHAR"),
	TYPE_CHAR		("TYPE_CHAR"),
	TYPE_INTEGER	("TYPE_INTEGER"),
	TYPE_NUMERIC	("TYPE_NUMERIC"),
	TYPE_DECIMAL	("TYPE_DECIMAL"),
	TYPE_DATETIME	("TYPE_DATETIME");

	private Types(String s) { }
}
