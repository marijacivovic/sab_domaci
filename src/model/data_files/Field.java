package model.data_files;

public class Field {

	private String name;
	private Types type;
	private int size;
	private boolean isPrimary;

	public Field(String name, Types type, int size, boolean isPrimary) {
		this.name = name;
		this.type = type;
		this.size = size;
		this.isPrimary = isPrimary;
	}

	public String getName() {
		return name;
	}

	public Types getType() {
		return type;
	}

	public int getSize() {
		return size;
	}

	public boolean getIsPrimary() {
		return isPrimary;
	}

	public String toDescriptionFile() {
		return String.format("field/%s/%s/%s/%s", name, type,
				String.valueOf(size), String.valueOf(isPrimary));
	}

	public String toFileString() {
		String s = "field/" + name + "/" + type + "/" + String.valueOf(size)
				+ "/" + String.valueOf(isPrimary);
		return s;
	}

	@Override
	public String toString() {
		return getName();
	}

}
