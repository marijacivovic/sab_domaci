package model.data_files;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import model.tree.KeyElement;
import model.tree.Node;
import model.tree.Tree;
import observer.ExpandPathListener;
import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;
import observer.UpdateBlockEvent;

public class IndexDataFile extends DataFileBase {

	private Tree indexTree;

	public IndexDataFile(File headerFile) {
		super(headerFile);
	}

	@Override
	public void readHeader() throws IOException {
		super.readHeader();
		readTree();
	}

	private void readTree() {
		ObjectInputStream os = null;

		try {
			os = new ObjectInputStream(
					new FileInputStream(header.getTreeFile()));
		} catch (Exception e) {
			System.out.println("Error opening index file stream");
			e.printStackTrace();
			return;
		}

		try {
			indexTree = (Tree) os.readObject();
			os.close();
		} catch (Exception e) {
			System.out.println("Error reading tree object");
			e.printStackTrace();
			return;
		}

	}

	public Tree getTree() {
		return indexTree;
	}

	public void loadBlock(int blockAddress) throws IOException {
		setFILE_POINTER(blockAddress * header.getRecordSize());
		fetchNextBlock();
	}

	private void updateFileStats(RandomAccessFile file) throws IOException {
		setFILE_SIZE(file.length());

		int recordSize = header.getRecordSize();

		if (getFILE_SIZE() % recordSize == 0) {
			setRECORD_NUM(getFILE_SIZE() / recordSize);
		} else {
			setRECORD_NUM(1 + getFILE_SIZE() / recordSize);
		}

		if (getRECORD_NUM() % getBLOCK_FACTOR() == 0) {
			setBLOCK_NUM(getRECORD_NUM() / getBLOCK_FACTOR());
		} else {
			setBLOCK_NUM(1 + getRECORD_NUM() / getBLOCK_FACTOR());
		}

	}

	@Override
	public boolean fetchNextBlock() throws IOException {
		RandomAccessFile afile = new RandomAccessFile(header.getSourceFile(),
				"r");

		updateFileStats(afile);

		int recordSize = header.getRecordSize();

		if (getFILE_POINTER() / recordSize + getBLOCK_FACTOR() > getRECORD_NUM())
			setBUFFER_SIZE((int) (getRECORD_NUM() - getFILE_POINTER()
					/ recordSize)
					* recordSize);
		else
			setBUFFER_SIZE((int) (recordSize * getBLOCK_FACTOR()));

		buffer = new byte[getBUFFER_SIZE()];

		afile.seek(getFILE_POINTER());
		setBufferStart(getFILE_POINTER());
		afile.read(buffer);

		try {
			createDataFromBuffer(buffer, recordSize);
		} catch (Exception ex) {
			System.out.println("WTF");
			System.out.println(ex);
		}
		setFILE_POINTER(afile.getFilePointer());

		afile.close();

		notifyUpdateBlockEventListeners(new UpdateBlockEvent(data));

		return true;
	}

	private String[] getRecordAt(int index, String content, int recordSize) {
		String line = content.substring(index * recordSize, index * recordSize
				+ recordSize);

		String[] row = new String[header.getFieldCount()];

		int k = 0;
		for (int j = 0; j < header.getFieldCount(); j++) {
			String field = null;
			field = line.substring(k, k + header.getFields()[j].getSize());
			row[j] = field;
			k = k + header.getFields()[j].getSize();
		}

		return row;
	}

	private void createDataFromBuffer(byte[] buffer, int recordSize) {
		String content = new String(buffer);

		if (content.length() < buffer.length) {
			for (int x = content.length(); x < buffer.length; x++)
				content = content + " ";
		}

		int actualRecords = 0;
		for (int i = 0; i < getBUFFER_SIZE(); i += recordSize) {
			actualRecords++;
		}

		data = new String[actualRecords][];
		int currentActualRow = 0;

		for (int i = 0; i < getBUFFER_SIZE() / recordSize; i++) {
			String[] record = getRecordAt(i, content, recordSize);
			if (record != null) {
				data[currentActualRow++] = getRecordAt(i, content, recordSize);
			}
		}
	}

	@Override
	public boolean addRecord(ArrayList<String> record) throws IOException {
		return true;
	}

	@Override
	public void flush() throws IOException {

	}

	@Override
	public boolean updateRecord(ArrayList<String> record) throws IOException {
		return false;
	}

	private int compareRows(ArrayList<String> rowPK,
			ArrayList<String> searchPK, ArrayList<Field> fields) {

		for (int i = 0; i < rowPK.size(); i++) {
			String s1 = rowPK.get(i).trim().toUpperCase();
			String s2 = searchPK.get(i).trim().toUpperCase();
			int cmp = compareValues(s1, s2, fields.get(i));
			if (cmp < 0) {
				return -1;
			} else if (cmp > 0) {
				return 1;
			}
		}
		return 0;
	}

	private boolean findSearchRange(ArrayList<String> pkSet,
			ArrayList<Field> fields, int[] range) {

		if (getRECORD_NUM() == 0) {
			return false;
		}

		range[0] = 0;

		ArrayList<Node> nodePath = new ArrayList<Node>();

		Node currentNode = indexTree.getRootElement();

		nodePath.add(currentNode);

		while (!currentNode.isLeaf()) {
			int nextNodeIndex = 0;

			while (nextNodeIndex < currentNode.getData().size()) {
				ArrayList<String> pk = new ArrayList<String>();
				for (KeyElement keyEl : currentNode.getData()
						.get(nextNodeIndex).getKeyValue()) {
					pk.add(keyEl.getValue().toString());
				}

				if (compareRows(pk, pkSet, fields) <= 0) {
					nextNodeIndex++;
				} else {
					break;
				}
			}

			nextNodeIndex--;
			if (nextNodeIndex == -1) {
				return false;
			} else {
				currentNode = (Node) currentNode.getChildAt(nextNodeIndex);
				nodePath.add(currentNode);
			}
		}

		notifyExpandPathListeners(nodePath);

		int dataIndex = 0;

		while (dataIndex < currentNode.getData().size()) {
			ArrayList<String> pk = new ArrayList<String>();
			for (KeyElement keyEl : currentNode.getData().get(dataIndex)
					.getKeyValue()) {
				pk.add(keyEl.getValue().toString());
			}

			if (compareRows(pk, pkSet, fields) <= 0) {
				dataIndex++;
			} else {
				break;
			}
		}

		dataIndex = dataIndex - 1;

		if (dataIndex == -1) {
			return false;
		}

		range[0] = currentNode.getData().get(dataIndex).getBlockAddress();

		return true;

	}

	@Override
	public boolean findRecord(ArrayList<String> searchRec, int[] position) {
		if (searchRec == null || searchRec.size() != header.getFieldCount()) {
			return false;
		}

		ArrayList<String> primaryKeySet = new ArrayList<String>();
		ArrayList<Field> primaryKeyFields = new ArrayList<Field>();

		int i = 0;
		for (String s : searchRec) {
			if (header.getFields()[i].getIsPrimary()) {
				primaryKeySet.add(s);
				primaryKeyFields.add(header.getFields()[i]);
			}
			i++;
		}

		int[] searchRange = new int[1];

		if (!findSearchRange(primaryKeySet, primaryKeyFields, searchRange)) {
			return false;
		}

		int initialSearchPosition = searchRange[0];

		setFILE_POINTER(initialSearchPosition * header.getRecordSize());

		while (getFILE_POINTER() < getFILE_SIZE()) {
			try {
				fetchNextBlock();
			} catch (Exception e) {
				return false;
			}

			for (int row = 0; row < data.length; row++) {
				notifySelectedItemChangedEventListeners(new SelectedItemChangedEvent<Integer>(
						row));

				System.out.println("Selecting row: " + row);

				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
				}

				ArrayList<String> rowPK = new ArrayList<String>();

				for (int j = 0; j < data[row].length; j++) {
					if (header.getFields()[j].getIsPrimary()) {
						rowPK.add(data[row][j]);
					}
				}

				int compareResult = compareRows(primaryKeySet, rowPK,
						primaryKeyFields);

				if (compareResult == 0) {
					position[0] = row;
					return true;
				} else if (compareResult < 0) {
					return false;
				}

			}

		}

		return false;
	}

	@Override
	public boolean deleteRecord(ArrayList<String> searchRec) {
		return false;
	}

	private ArrayList<SelectedItemChangedEventListener<Integer>> selectedItemChangedEventListeners = new ArrayList<SelectedItemChangedEventListener<Integer>>();

	public void addSelectedItemChangedListener(
			SelectedItemChangedEventListener<Integer> listener) {
		if (listener != null) {
			selectedItemChangedEventListeners.add(listener);
		}
	}

	public void removeSelectedItemChangedListener(
			SelectedItemChangedEventListener<Integer> listener) {
		selectedItemChangedEventListeners.remove(listener);
	}

	private void notifySelectedItemChangedEventListeners(
			SelectedItemChangedEvent<Integer> event) {
		for (SelectedItemChangedEventListener<Integer> listener : selectedItemChangedEventListeners) {
			listener.selectedItemChanged(this, event);
		}
	}

	private ArrayList<ExpandPathListener> expandPathListeners = new ArrayList<ExpandPathListener>();

	public void addExpandPathListener(ExpandPathListener listener) {
		expandPathListeners.add(listener);
	}

	public void removeExpandPathListener(ExpandPathListener listener) {
		expandPathListeners.remove(listener);
	}

	private void notifyExpandPathListeners(ArrayList<Node> nodePath) {
		for (ExpandPathListener listener : expandPathListeners) {
			if (listener != null) {
				listener.expand(nodePath);
			}
		}
	}
}
