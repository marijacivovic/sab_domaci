package model.data_files;

import javax.swing.table.DefaultTableModel;

import observer.UpdateBlockEvent;
import observer.UpdateBlockEventListener;


public class DataTableModel extends DefaultTableModel implements
		UpdateBlockEventListener {

	private static final long serialVersionUID = -7170215527127991676L;

	private IDataFile dataFile;

	public DataTableModel(IDataFile dataFile) {
		super(dataFile.getFileHeader().getFields(), 10);
		setColumnIdentifiers(dataFile.getFileHeader().getFields());
		this.dataFile = dataFile;
		dataFile.addUpdateBlockEventListener(this);
	}

	public IDataFile getDataFile() {
		return dataFile;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public void blockUpdated(UpdateBlockEvent e) {
		while (getRowCount() > 0) {
			removeRow(getRowCount() - 1);
		}
		if (e != null && e.getData() != null) {
			for (Object[] row : e.getData()) {
				addRow(row);
			}
		}
	}

}
