package view.viewers.db_table_viewer;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;

public class DBTableTable extends JTable implements
		SelectedItemChangedEventListener<Integer> {

	private static final long serialVersionUID = -1541835571285586996L;

	public DBTableTable() {
		super();
		setDefaultRenderer(Object.class, new DBTableTableRenderer());
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setShowGrid(true);
		getTableHeader().setReorderingAllowed(false);
	}

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<Integer> e) {

		if (e != null) {
			int row = e.getSelectedItem();
			getSelectionModel().setSelectionInterval(row, row);
		}

	}

}
