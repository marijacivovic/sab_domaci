package view.viewers.db_table_viewer;

import gui.MainWindow;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.db.DBColumn;
import model.db.DBDataTypes;

public class DBFilterRecordsDialog extends JDialog implements ItemListener {

	private static final long serialVersionUID = -8125101555511152281L;

	public static final int ADD_MODE = 0;
	public static final int FIND_MODE = 1;
	public static final int SEQ_EDIT_MODE = 5;
	public static final int PK_FIND_MODE = 6;

	private HashMap<String, JComboBox<String>> compareFields = new HashMap<String, JComboBox<String>>();
	private HashMap<String, JTextField> inputFields = new HashMap<String, JTextField>();

	private ArrayList<String[]> result;

	private int selectedFields = 0;
	private JButton btnOK;

	public DBFilterRecordsDialog(String title, final DBColumn[] columns,
			final Object[] initialValues, int mode) {
		super(MainWindow.getInstance(), title, true);

		setModal(true);

		setLayout(new BorderLayout(4, 4));

		int gridHeight = columns.length;

		JPanel fieldTitlesPanel = new JPanel();
		fieldTitlesPanel.setLayout(new GridLayout(gridHeight, 2, 4, 4));

		JPanel fieldInputsPanel = new JPanel();
		fieldInputsPanel.setLayout(new GridLayout(gridHeight, 1, 4, 4));

		for (int i = 0; i < columns.length; i++) {
			final DBColumn column = columns[i];

			JTextField inputField = new JTextField(50);

			if (initialValues != null && i < initialValues.length) {
				Object value = initialValues[i];
				if (value != null) {
					inputField.setText(initialValues[i].toString());
				}
			}

			if (column.getIsPrimaryKey()) {
				inputField.setBackground(Color.GRAY);
				inputField.setForeground(Color.WHITE);
			}

			inputFields.put(column.getName(), inputField);

			fieldTitlesPanel.add(new JLabel(column.getName(), JLabel.RIGHT));

			JComboBox<String> comboBox = formComboBox(column);
			compareFields.put(column.getName(), comboBox);
			fieldTitlesPanel.add(comboBox);
			comboBox.addItemListener(this);

			JPanel inputFieldWrapper = new JPanel();
			inputFieldWrapper.setLayout(new FlowLayout(FlowLayout.LEADING));
			inputFieldWrapper.add(inputField);
			fieldInputsPanel.add(inputFieldWrapper);
		}

		add(fieldTitlesPanel, BorderLayout.WEST);
		add(fieldInputsPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

		btnOK = new JButton("Ok");
		btnOK.setEnabled(false);
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				result = new ArrayList<String[]>();
				for (int i = 0; i < columns.length; i++) {
					String colName = columns[i].getName();
					if (!compareFields.get(colName).getSelectedItem()
							.equals("")) {
						String[] item = new String[3];
						item[0] = colName;
						item[1] = compareFields.get(colName).getSelectedItem()
								.toString();
						item[2] = getFieldValue(colName);
						result.add(item);
					}
				}
				setVisible(false);
			}
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				result = null;
				setVisible(false);
			}
		});

		JPanel tempPanel = new JPanel();
		tempPanel.setLayout(new GridLayout(1, 2, 10, 0));
		tempPanel.add(btnOK);
		tempPanel.add(btnCancel);
		buttonsPanel.add(tempPanel);
		add(buttonsPanel, BorderLayout.SOUTH);

		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private JComboBox<String> formComboBox(DBColumn column) {
		Vector<String> items = new Vector<String>();
		items.add("");
		items.add("=");
		DBDataTypes type = DBDataTypes.valueOf(column.getType().toUpperCase());
		if (type.isLikeCompareable()) {
			items.add("like");
		}
		if (type.isLessCompareable()) {
			items.add("<");
			items.add("<=");
			items.add(">");
			items.add(">=");
		}
		JComboBox<String> comboBox = new JComboBox<String>(items);
		comboBox.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
		return comboBox;
	}

	private String getFieldValue(String columnName) {
		String fieldValue = inputFields.get(columnName).getText();
		return fieldValue;
	}

	public ArrayList<String[]> getResult() {
		return result;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (!(e.getStateChange() == ItemEvent.SELECTED)) {
			return;
		}

		if (e.getItem().equals("")) {
			selectedFields--;
		} else {
			selectedFields++;
		}

		btnOK.setEnabled(selectedFields > 0);
	}

}
