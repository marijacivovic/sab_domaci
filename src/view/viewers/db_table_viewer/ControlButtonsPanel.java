package view.viewers.db_table_viewer;

import gui.MainWindow;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.db.DBForeignKey;
import model.db.DBTableFile;
import view.viewers.db_table_viewer.sort_dialog.SortRecordsDialog;

public class ControlButtonsPanel extends JPanel implements
		ListSelectionListener {

	private static final long serialVersionUID = 4271862091013146970L;

	private DBTableTable table;
	private DBTableFile tableFile;
	private DBForeignKey foreignKey;

	private JButton addRecordButton;
	private JButton deleteRecordButton;
	private JButton editRecordButton;
	private JButton filterRecordsButton;
	private JButton sortRecordsButton;

	public ControlButtonsPanel(DBTableTable table, DBTableFile tableFile,
			DBTableTable dataTable, DBForeignKey foreignKey) {
		this.table = table;
		this.tableFile = tableFile;
		this.foreignKey = foreignKey;

		setLayout(new FlowLayout(FlowLayout.RIGHT));

		addRecordButton = new JButton("Add Record");
		editRecordButton = new JButton("Edit Record");
		deleteRecordButton = new JButton("Delete Record");
		filterRecordsButton = new JButton("Filter Records");
		sortRecordsButton = new JButton("Sort Records");

		editRecordButton.setEnabled(false);
		deleteRecordButton.setEnabled(false);

		addRecordButton.addActionListener(listener);
		editRecordButton.addActionListener(listener);
		deleteRecordButton.addActionListener(listener);
		filterRecordsButton.addActionListener(listener);
		sortRecordsButton.addActionListener(listener);

		add(addRecordButton);
		add(editRecordButton);
		add(deleteRecordButton);
		add(filterRecordsButton);
		add(sortRecordsButton);

		table.getSelectionModel().addListSelectionListener(this);
	}

	private void addRecordButtonClick() {
		DBRecordDialog dialog = new DBRecordDialog("Add Record", tableFile
				.getDBTable().getColumns(), null, 0);

		ArrayList<String> resultRecord = dialog.getResultRecord();

		if (resultRecord != null) {
			try {
				if (tableFile.addRecord(resultRecord)) {
					JOptionPane.showMessageDialog(MainWindow.getInstance(),
							"Record added!", "Add Record",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(MainWindow.getInstance(),
							"Adding record failed!", "Add Record",
							JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(MainWindow.getInstance(),
						ex.getMessage(), "Add Record",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void deleteRecordButtonClick() {
		Object[] selectedRow = tableFile.getRow(table.getSelectedRow());

		if (selectedRow == null) {
			return;
		}

		ArrayList<String> record = new ArrayList<>();
		for (Object object : selectedRow) {
			record.add(String.valueOf(object));
		}

		try {
			if (tableFile.deleteRecord(record)) {
				JOptionPane.showMessageDialog(MainWindow.getInstance(),
						"Record deleted!", "Delete Record",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(MainWindow.getInstance(),
						"Deleting record failed!", "Delete Record",
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception ex) {
			JOptionPane
					.showMessageDialog(MainWindow.getInstance(),
							ex.getMessage(), "Delete Record",
							JOptionPane.ERROR_MESSAGE);
		}
	}

	private void editRecordButtonClick() {
		Object[] selectedRow = tableFile.getRow(table.getSelectedRow());

		if (selectedRow == null) {
			return;
		}

		ArrayList<String> record = new ArrayList<>();
		for (Object object : selectedRow) {
			record.add(String.valueOf(object));
		}

		DBRecordDialog dialog = new DBRecordDialog("Edit Record", tableFile
				.getDBTable().getColumns(), selectedRow, 0, foreignKey);

		if (dialog.getResultRecord() != null) {
			try {
				if (tableFile.updateRecord(record, dialog.getResultRecord())) {
					JOptionPane.showMessageDialog(MainWindow.getInstance(),
							"Record updated!", "Update Record",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(MainWindow.getInstance(),
							"Updating record failed!", "Update Record",
							JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(MainWindow.getInstance(),
						ex.getMessage(), "Update Record",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void filterRecordsButtonClick() {
		DBFilterRecordsDialog dialog = new DBFilterRecordsDialog(
				"Filter Records", tableFile.getDBTable().getColumns(), null, 0);

		ArrayList<String[]> result = dialog.getResult();

		if (result != null) {
			try {
				tableFile.filterRecords(result);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(MainWindow.getInstance(),
						ex.getMessage(), "Filter Records",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void sortRecordsButtonClick() {
		SortRecordsDialog dialog = new SortRecordsDialog(tableFile.getDBTable()
				.getColumns());
		ArrayList<String[]> sortParams = dialog.getValues();
		if (sortParams != null) {
			tableFile.sortRecords(sortParams);
		}
	}

	private ActionListener listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(addRecordButton)) {
				addRecordButtonClick();
			} else if (e.getSource().equals(deleteRecordButton)) {
				deleteRecordButtonClick();
			} else if (e.getSource().equals(editRecordButton)) {
				editRecordButtonClick();
			} else if (e.getSource().equals(filterRecordsButton)) {
				filterRecordsButtonClick();
			} else if (e.getSource().equals(sortRecordsButton)) {
				sortRecordsButtonClick();
			}
		}
	};

	private int lastSelectedItem = Integer.MIN_VALUE;

	@Override
	public void valueChanged(ListSelectionEvent e) {
		int selectedItem = table.getSelectedRow();

		if (selectedItem == lastSelectedItem) {
			return;
		}

		lastSelectedItem = selectedItem;

		editRecordButton.setEnabled(selectedItem > -1);
		deleteRecordButton.setEnabled(selectedItem > -1);
	}

}
