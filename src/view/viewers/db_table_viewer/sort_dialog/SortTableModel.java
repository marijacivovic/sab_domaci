package view.viewers.db_table_viewer.sort_dialog;

import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;

import model.db.DBColumn;

public class SortTableModel extends DefaultTableModel {

	private static final long serialVersionUID = -4739527969260908544L;

	private DBColumn[] columns;

	public SortTableModel(DBColumn[] columns) {
		super(new String[] { "Column", "Order" }, 0);
		this.columns = columns;
	}

	public void createNewRow() {
		String labelText = (getRowCount() == 0) ? "  Order by:  "
				: "  Then by:    ";
		JComboBox<String> combo = new JComboBox<String>(new String[] {
				"Ascending", "Descending" });
		String[] columnNames = new String[columns.length];
		for (int i = 0; i < columnNames.length; i++) {
			columnNames[i] = columns[i].getName();
		}
		addRow(new Object[] { new ColumnChoosePanel(labelText, columnNames),
				combo });
	}

	@Override
	public void removeRow(int rowNumber) {
		super.removeRow(rowNumber);
		if (getRowCount() > 0) {
			((ColumnChoosePanel) getValueAt(0, 0)).setLabel("  Order by:  ");
		}
	}

	public ArrayList<String[]> getValues() {
		ArrayList<String[]> values = new ArrayList<>();
		for (int i = 0; i < getRowCount(); i++) {
			String[] pair = new String[2];
			pair[0] = ((ColumnChoosePanel) getValueAt(i, 0)).getSelectedItem()
					.toString();
			pair[1] = ((JComboBox<?>) getValueAt(i, 1)).getSelectedItem()
					.toString();
			values.add(pair);
		}
		return values;
	}

}
