package view.viewers.db_table_viewer.sort_dialog;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ColumnChoosePanel extends JPanel {

	private static final long serialVersionUID = 8945697370526309794L;

	private JLabel titleLabel;
	private JComboBox<String> combo;

	public ColumnChoosePanel(String title, String[] columns) {
		setLayout(new BorderLayout());
		titleLabel = new JLabel(title);
		add(titleLabel, BorderLayout.WEST);
		combo = new JComboBox<String>(columns);
		add(combo, BorderLayout.CENTER);
	}

	public void setLabel(String value) {
		titleLabel.setText(value);
	}

	public Object getSelectedItem() {
		return combo.getSelectedItem();
	}

	public void setSelectedItem(Object item) {
		combo.setSelectedItem(item);
	}

	public void setSelected(boolean value, Color selectionColor) {
		Color backColor = value ? (selectionColor) : (new JPanel())
				.getBackground();
		setBackground(backColor);
		combo.setBackground(backColor);
	}
}
