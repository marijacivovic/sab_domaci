package view.viewers.db_table_viewer.sort_dialog;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;

public class ColumnChooseCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 7023249592556578114L;

	private ColumnChoosePanel lastPanel;
	public ColumnChooseCellEditor() {
		super(new JTextField());
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSel, int row, int col) {
		ColumnChoosePanel panel = (ColumnChoosePanel) value;
		lastPanel = panel;
		return panel;
	}
	
	@Override
	public Object getCellEditorValue() {
		return lastPanel.getSelectedItem();
	}

}
