package view.viewers.db_table_viewer.sort_dialog;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;

public class OrderTypeCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 1201996733943485890L;

	public OrderTypeCellEditor() {
		super(new JComboBox<>(new String[] { "Ascending", "Descending" }));
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSel, int row, int col) {
		JComboBox<?> editValue = (JComboBox<?>) value;
		JComboBox<?> res = (JComboBox<?>) super.getTableCellEditorComponent(
				table, value, isSel, row, col);
		res.setSelectedItem(editValue.getSelectedItem());
		return res;
	}
}
