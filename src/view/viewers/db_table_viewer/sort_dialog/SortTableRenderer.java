package view.viewers.db_table_viewer.sort_dialog;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class SortTableRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 5487816885380562322L;

	public SortTableRenderer() {

	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		Color selectedColor = table.getSelectionBackground();

		if (value instanceof ColumnChoosePanel) {
			((ColumnChoosePanel) value).setSelected(isSelected, selectedColor);
			return (ColumnChoosePanel) value;
		}

		if (value instanceof JComboBox<?>) {
			((JComboBox<?>) value).setBackground(isSelected ? (selectedColor)
					: (new JComboBox<>()).getBackground());
			return (JComboBox<?>) value;
		}

		Component superResult = super.getTableCellRendererComponent(table,
				value, isSelected, hasFocus, row, column);

		return superResult;
	}
}
