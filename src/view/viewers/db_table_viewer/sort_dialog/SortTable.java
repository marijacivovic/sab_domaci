package view.viewers.db_table_viewer.sort_dialog;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

public class SortTable extends JTable {

	private static final long serialVersionUID = -2113259781766603032L;

	public SortTable() {
		getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
		setDefaultRenderer(Object.class, new SortTableRenderer());
	}

	public void setEditors() {
		TableColumn columnChoose = getColumn(getColumnName(0));
		columnChoose.setCellEditor(new ColumnChooseCellEditor());
		TableColumn orderTypeColumn = getColumn(getColumnName(1));
		orderTypeColumn.setCellEditor(new OrderTypeCellEditor());
	}

	@Override
	public void setValueAt(Object value, int row, int column) {
		if (getValueAt(row, column) instanceof JComboBox<?>) {
			JComboBox<?> combo = (JComboBox<?>) getValueAt(row, column);
			combo.setSelectedItem(value);
			return;
		}
		if (getValueAt(row, column) instanceof ColumnChoosePanel) {
			ColumnChoosePanel panel = (ColumnChoosePanel) getValueAt(row,
					column);

			panel.setSelectedItem(value);
		}
	}

	@Override
	public TableCellEditor getCellEditor(int row, int column) {
		return super.getCellEditor(row, column);
	}
}
