package view.viewers.db_table_viewer.sort_dialog;

import gui.MainWindow;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.db.DBColumn;

public class SortRecordsDialog extends JDialog implements ListSelectionListener {

	private static final long serialVersionUID = -8409655532439759288L;

	private DBColumn[] columns;

	private JButton addLevelButton;
	private JButton deleteLevelButton;

	private SortTable table;
	private SortTableModel tableModel;

	private JButton okButton;
	private JButton cancelButton;

	private ArrayList<String[]> values;

	public SortRecordsDialog(DBColumn[] columns) {
		super(MainWindow.getInstance(), "Sort Records", true);

		this.columns = columns;

		setLayout(new BorderLayout(10, 10));

		createLevelButtons();

		createCenterPanel();

		createOkCloseButtons();

		pack();
		setSize(400, 400);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}

	private void createLevelButtons() {
		addLevelButton = new JButton("Add Level");
		deleteLevelButton = new JButton("Delete Level");

		addLevelButton.addActionListener(clickListener);
		deleteLevelButton.addActionListener(clickListener);

		deleteLevelButton.setEnabled(false);

		JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel gridPanel = new JPanel(new GridLayout(1, 2, 5, 5));

		gridPanel.add(addLevelButton);
		gridPanel.add(deleteLevelButton);

		northPanel.add(gridPanel);
		add(northPanel, BorderLayout.NORTH);
	}

	private void createCenterPanel() {
		JPanel centerPanel = new JPanel(new BorderLayout());

		table = new SortTable();
		tableModel = new SortTableModel(columns);
		table.setModel(tableModel);
		table.setEditors();
		table.getSelectionModel().addListSelectionListener(this);

		JScrollPane tableScrollPane = new JScrollPane(table);
		tableScrollPane
				.setBorder(BorderFactory.createLineBorder(Color.GRAY, 2));

		centerPanel.add(tableScrollPane, BorderLayout.CENTER);

		centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

		add(centerPanel, BorderLayout.CENTER);
	}

	private void createOkCloseButtons() {
		okButton = new JButton("Ok");
		cancelButton = new JButton("Cancel");

		okButton.addActionListener(clickListener);
		cancelButton.addActionListener(clickListener);

		okButton.setEnabled(false);

		JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JPanel gridPanel = new JPanel(new GridLayout(1, 2, 5, 5));

		gridPanel.add(okButton);
		gridPanel.add(cancelButton);

		southPanel.add(gridPanel);
		add(southPanel, BorderLayout.SOUTH);
	}

	private void addLevelButtonClick() {
		tableModel.createNewRow();
		okButton.setEnabled(true);
	}

	private void deleteLevelButtonClick() {
		int selectedRow = table.getSelectedRow();
		if (selectedRow == -1) {
			return;
		}
		tableModel.removeRow(selectedRow);

		if (table.getRowCount() > 0) {
			if (selectedRow < table.getRowCount()) {
				table.getSelectionModel().setSelectionInterval(selectedRow,
						selectedRow);
			} else {
				table.getSelectionModel().setSelectionInterval(
						table.getRowCount() - 1, table.getRowCount() - 1);
			}
		} else {
			okButton.setEnabled(false);
			deleteLevelButton.setEnabled(false);
		}
	}

	private void okButtonClick() {
		values = tableModel.getValues();
		setVisible(false);
	}

	private void cancelButtonClick() {
		values = null;
		setVisible(false);
	}

	private ActionListener clickListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(addLevelButton)) {
				addLevelButtonClick();
			} else if (e.getSource().equals(deleteLevelButton)) {
				deleteLevelButtonClick();
			} else if (e.getSource().equals(okButton)) {
				okButtonClick();
			} else if (e.getSource().equals(cancelButton)) {
				cancelButtonClick();
			}
		}
	};

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) {
			return;
		}
		deleteLevelButton.setEnabled(e.getFirstIndex() != -1);
	}

	public ArrayList<String[]> getValues() {
		return values;
	}

}
