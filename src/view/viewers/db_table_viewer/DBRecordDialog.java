package view.viewers.db_table_viewer;

import gui.MainWindow;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.db.DBColumn;
import model.db.DBForeignKey;

public class DBRecordDialog extends JDialog {

	private static final long serialVersionUID = -8125101555511152281L;

	public static final int ADD_MODE = 0;
	public static final int FIND_MODE = 1;
	public static final int SEQ_EDIT_MODE = 5;
	public static final int PK_FIND_MODE = 6;

	private HashMap<String, JTextField> inputFields = new HashMap<String, JTextField>();

	private ArrayList<String> resultRecord;

	public DBRecordDialog(String title, final DBColumn[] columns,
			final Object[] initialValues, int mode) {
		super(MainWindow.getInstance(), title, true);
		initialize(columns, initialValues, null);
	}

	public DBRecordDialog(String title, final DBColumn[] columns,
			final Object[] initialValues, int mode, DBForeignKey foreignKey) {
		super(MainWindow.getInstance(), title, true);
		initialize(columns, initialValues, foreignKey);
	}

	private void initialize(final DBColumn[] columns, Object[] initialValues,
			DBForeignKey foreignKey) {
		setModal(true);
		setLayout(new BorderLayout(4, 4));

		int gridHeight = columns.length;

		JPanel fieldTitlesPanel = new JPanel();
		fieldTitlesPanel.setLayout(new GridLayout(gridHeight, 1, 4, 4));

		JPanel fieldInputsPanel = new JPanel();
		fieldInputsPanel.setLayout(new GridLayout(gridHeight, 1, 4, 4));

		for (int i = 0; i < columns.length; i++) {
			final DBColumn column = columns[i];

			JTextField inputField = new JTextField(50);

			if (initialValues != null && i < initialValues.length) {
				Object value = initialValues[i];
				if (value != null) {
					inputField.setText(initialValues[i].toString());
				}
			}

			if (column.getIsPrimaryKey()) {
				inputField.setBackground(Color.GRAY);
				inputField.setForeground(Color.WHITE);
			}

			if (foreignKey != null) {
				for (int j = 0; j < foreignKey.getColumnCount(); j++) {
					if (foreignKey.getChildColumnAt(j) == column) {
						inputField.setBackground(Color.RED);
						inputField.setEditable(false);
					}
				}
			}

			inputFields.put(column.getName(), inputField);

			fieldTitlesPanel.add(new JLabel(column.getName(), JLabel.RIGHT));
			JPanel inputFieldWrapper = new JPanel();
			inputFieldWrapper.setLayout(new FlowLayout(FlowLayout.LEADING));
			inputFieldWrapper.add(inputField);
			fieldInputsPanel.add(inputFieldWrapper);
		}

		add(fieldTitlesPanel, BorderLayout.WEST);
		add(fieldInputsPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

		JButton btnOK = new JButton("Ok");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resultRecord = new ArrayList<String>();
				for (int i = 0; i < columns.length; i++) {
					resultRecord.add(getFieldValue(columns[i].getName()));
				}
				setVisible(false);
			}
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resultRecord = null;
				setVisible(false);
			}
		});

		JPanel tempPanel = new JPanel();
		tempPanel.setLayout(new GridLayout(1, 2, 10, 0));
		tempPanel.add(btnOK);
		tempPanel.add(btnCancel);
		buttonsPanel.add(tempPanel);
		add(buttonsPanel, BorderLayout.SOUTH);

		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private String getFieldValue(String columnName) {
		String fieldValue = inputFields.get(columnName).getText();
		return fieldValue;
	}

	public ArrayList<String> getResultRecord() {
		return resultRecord;
	}

}
