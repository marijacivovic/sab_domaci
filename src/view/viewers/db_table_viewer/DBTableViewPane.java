package view.viewers.db_table_viewer;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.db.DBForeignKey;
import model.db.DBTable;
import model.db.DBTableFile;
import model.db.DBTableTableModel;

public class DBTableViewPane extends JPanel {

	private static final long serialVersionUID = -6434815187664015590L;

	private ControlButtonsPanel controlButtonsPanel;
	private DBTableTable dataTable;
	private DBTableFile tableFile;
	private DBTable table;
	private DBForeignKey foreignKey;

	private ArrayList<DBTableViewPane> childPanes;

	public DBTableViewPane(DBTable table) {
		this(table, null);
	}

	public DBTableViewPane(DBTable table, DBForeignKey fk) {
		this.table = table;
		this.foreignKey = fk;
		initialize();
	}

	private void initialize() {
		tableFile = new DBTableFile(table);
		if (foreignKey != null) {
			tableFile.setForeignKey(foreignKey);
		}

		JPanel masterPane = new JPanel(new BorderLayout());

		DBTableTableModel dbtableTableModel = new DBTableTableModel(tableFile);
		tableFile.addUpdateBlockEventListener(dbtableTableModel);

		dataTable = new DBTableTable();
		dataTable.setModel(dbtableTableModel);
		tableFile.addSelectedItemChangedEventListener(dataTable);

		controlButtonsPanel = new ControlButtonsPanel(dataTable, tableFile,
				dataTable, foreignKey);

		setLayout(new BorderLayout());
		masterPane.add(controlButtonsPanel, BorderLayout.NORTH);
		masterPane.add(new JScrollPane(dataTable), BorderLayout.CENTER);

		try {
			tableFile.fetchNextBlock();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		addSelectedItemChangeListener(masterSelectionListener);

		childPanes = new ArrayList<>();
		JTabbedPane childTabbedPane = new JTabbedPane();
		for (int i = 0; i < table.getChildFKCount(); i++) {
			DBForeignKey fk = table.getChildFKAt(i);
			DBTableViewPane tempChildPane = new DBTableViewPane(
					fk.getChildTable(), fk);
			childPanes.add(tempChildPane);
			childTabbedPane.addTab(childPanes.get(i).getTableName(),
					childPanes.get(i));
		}

		if (childPanes.size() > 0) {
			JPanel masterWrapper = new JPanel(new BorderLayout());
			masterWrapper.add(masterPane, BorderLayout.CENTER);

			JPanel tabsWrapper = new JPanel(new BorderLayout());
			tabsWrapper.add(childTabbedPane, BorderLayout.CENTER);

			JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
					masterWrapper, tabsWrapper);
			splitPane.setDividerLocation(200);

			add(splitPane, BorderLayout.CENTER);
		} else {
			add(masterPane, BorderLayout.CENTER);
		}
	}

	public String getTableName() {
		return table.getName();
	}

	public void addSelectedItemChangeListener(ListSelectionListener listener) {
		dataTable.getSelectionModel().addListSelectionListener(listener);
	}

	public void removeSelectedItemChangeListener(ListSelectionListener listener) {
		dataTable.getSelectionModel().removeListSelectionListener(listener);
	}

	public Object[] getMasterData() {
		int row = dataTable.getSelectedRow();
		return tableFile.getRow(row);
	}

	public void setMasterData(Object[] data) {
		if (foreignKey != null) {
			tableFile.setMasterData(data);
		}
	}

	private ListSelectionListener masterSelectionListener = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				Object[] masterData = getMasterData();
				for (DBTableViewPane child : childPanes) {
					child.setMasterData(masterData);
				}
			}
		}
	};

}
