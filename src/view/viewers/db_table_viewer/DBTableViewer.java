package view.viewers.db_table_viewer;

import java.awt.BorderLayout;

import model.SourceArtifact;
import model.db.DBTable;
import view.viewers.ViewerBase;

public class DBTableViewer extends ViewerBase<DBTable> {

	private static final long serialVersionUID = 7901207433191302015L;

	private DBTableViewPane masterPane;

	public DBTableViewer(DBTable table) {
		super(table);
	}

	@Override
	public String getViewerName() {
		return null;
	}

	@Override
	public void load() {
		setLayout(new BorderLayout());
		masterPane = new DBTableViewPane(getSource());
		add(masterPane);
	}

	@Override
	protected boolean tryClose() {
		return true;
	}

	@Override
	public ViewerBase<?> getClone(SourceArtifact source) {
		if (source instanceof DBTable) {
			return new DBTableViewer((DBTable) source);
		}
		return null;
	}

}
