package view.viewers;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.Saveable;
import model.SourceArtifact;
import observer.EventArgs;
import observer.Observer;
import observer.EventArguments.PropertyChangedEventArgs;

public abstract class ViewerBase<T extends SourceArtifact> extends JPanel
		implements Saveable {

	private static final long serialVersionUID = -2914670253491516855L;

	private static int newDocuments = -1;

	private static int getNewDocumentId() {
		newDocuments++;
		return newDocuments - 1;
	}

	protected T source;

	protected String title;

	private boolean canSave;

	public ViewerBase(T source) {
		this.source = source;
		canSave = false;

		if (source == null) {
			title = "Untitled " + getNewDocumentId();
		} else {
			title = source.getName();
		}
	}

	public abstract String getViewerName();

	public T getSource() {
		return source;
	}

	public String getDisplayTitle() {
		return title;
	}

	public abstract void load();

	public abstract ViewerBase<?> getClone(SourceArtifact source);

	public boolean canSave() {
		return canSave;
	}

	public void setCanSave(boolean value) {
		if (value != canSave) {
			canSave = value;
			notifyObservers(new PropertyChangedEventArgs(Saveable.CAN_SAVE,
					value));
		}
	}

	public boolean save() {
		return true;
	}

	protected abstract boolean tryClose();

	protected void onChanged() {
		// notifyObservers(new FileChangedEventArgs());
		setCanSave(true);
	}

	public final boolean close() {
		if (canSave()) {
			int confirmResult = JOptionPane.showConfirmDialog(null,
					"Save file \"" + title + "\" ?", "Save",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.INFORMATION_MESSAGE);

			if (confirmResult == JOptionPane.YES_OPTION) {
				boolean saveResult = save();
				if (saveResult) {
					return tryClose();
				} else {
					return false;
				}
			} else if (confirmResult == JOptionPane.NO_OPTION) {
				return tryClose();
			} else if (confirmResult == JOptionPane.CANCEL_OPTION) {
				return false;
			} else {
				return false;
			}
		}
		return tryClose();
	}

	private ArrayList<Observer> observers = new ArrayList<Observer>();

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	protected void notifyObservers(EventArgs e) {
		for (Observer o : observers) {
			o.notifyObserver(this, e);
		}
	}

}
