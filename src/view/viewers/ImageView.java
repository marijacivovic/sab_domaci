package view.viewers;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import model.FileArtifact;
import model.SourceArtifact;

public class ImageView extends ViewerBase<FileArtifact> {

	private static final long serialVersionUID = 6301859716807990996L;

	private JLabel imageHolder;

	public ImageView(FileArtifact file) {
		super(file);
		setLayout(new BorderLayout());
		imageHolder = new JLabel();
		JScrollPane scrollPane = new JScrollPane(imageHolder);
		add(scrollPane, BorderLayout.CENTER);
	}

	@Override
	public String getViewerName() {
		return "ImageView";
	}

	@Override
	public void load() {
		imageHolder.setIcon(new ImageIcon(source.getFile().getAbsolutePath()));
	}

	@Override
	public ViewerBase<?> getClone(SourceArtifact source) {
		if (source instanceof FileArtifact) {
			return new ImageView((FileArtifact) source);
		}
		return null;
	}

	@Override
	public boolean save() {
		return true;
	}

	@Override
	public boolean tryClose() {
		return false;
	}

}
