package view.viewers.data_viewer;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

import model.FileArtifact;
import model.SourceArtifact;
import model.data_files.DataFileBase;
import model.data_files.DataTableModel;
import model.data_files.IndexDataFile;
import model.data_files.SequenceDataFile;
import model.data_files.SerialDataFile;
import util.Util;
import view.viewers.ViewerBase;
import actions.IndexTreeSelectionChangeListener;

public class DataView extends ViewerBase<FileArtifact> {

	private static final long serialVersionUID = 6245836041676174338L;

	private DataFileBase dataFile;

	private IndexTree tree;

	public DataView(FileArtifact file) {
		super(file);
	}

	@Override
	public String getViewerName() {
		return "DataView";
	}

	private void createPanels() {
		DataTable dataTable = new DataTable();

		dataTable.setModel(new DataTableModel(dataFile));

		DataFileDetailsPanel detailsPanel = new DataFileDetailsPanel(dataFile);
		ControlButtonsPanel controlsPanel = new ControlButtonsPanel(dataFile,
				dataTable);

		if (dataFile instanceof SerialDataFile) {
			((SerialDataFile) dataFile)
					.addSelectedItemChangedListener(dataTable);
		} else if (dataFile instanceof IndexDataFile) {
			((IndexDataFile) dataFile)
					.addSelectedItemChangedListener(dataTable);
		}

		if (dataFile instanceof IndexDataFile) {
			IndexDataFile indFile = (IndexDataFile) dataFile;
			TreeModel treeModel = new DefaultTreeModel(indFile.getTree()
					.getRootElement());
			tree = new IndexTree(treeModel);
			tree.addTreeSelectionListener(new IndexTreeSelectionChangeListener(
					indFile));
			indFile.addExpandPathListener(tree);
		}

		dataFile.addObserver(detailsPanel);
		dataTable.addSelectedItemChangedEventListener(controlsPanel);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(detailsPanel);
		add(controlsPanel);

		if (dataFile instanceof IndexDataFile) {
			JSplitPane dataPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
					new JScrollPane(tree), new JScrollPane(dataTable));
			dataPane.setDividerSize(5);
			dataPane.setDividerLocation(250);

			JPanel p = new JPanel(new BorderLayout());
			p.add(dataPane, BorderLayout.CENTER);
			add(p);
		} else {
			JPanel dataPanel = new JPanel();
			dataPanel.setLayout(new BorderLayout());
			dataPanel.add(new JScrollPane(dataTable), BorderLayout.CENTER);
			add(dataPanel);
		}

	}

	private void showFailWindow() {
		removeAll();
		setLayout(new BorderLayout());
		JLabel errorLabel = new JLabel("Failed to load data file.",
				JLabel.CENTER);
		add(errorLabel, BorderLayout.CENTER);
	}

	@Override
	public void load() {
		if (Util.getFileExtension(source.getFile()).equals(".SER")) {
			dataFile = new SerialDataFile(source.getFile());
		} else if (Util.getFileExtension(source.getFile()).equals(".SEK")) {
			dataFile = new SequenceDataFile(source.getFile());
		} else if (Util.getFileExtension(source.getFile()).equals(".IND")) {
			dataFile = new IndexDataFile(source.getFile());
		}
		try {
			dataFile.readHeader();
		} catch (Exception e) {
			System.out.println("Error loading header");
			System.out.println(e);
		}
		if (dataFile != null) {
			createPanels();
		} else {
			showFailWindow();
		}
		try {
			dataFile.fetchNextBlock();
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	@Override
	public ViewerBase<?> getClone(SourceArtifact source) {
		if (source instanceof FileArtifact) {
			return new DataView((FileArtifact) source);
		}
		return null;
	}

	@Override
	protected boolean tryClose() {
		return true;
	}

}
