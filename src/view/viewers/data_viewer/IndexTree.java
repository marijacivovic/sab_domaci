package view.viewers.data_viewer;

import java.util.ArrayList;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import model.tree.Node;

import observer.ExpandPathListener;

public class IndexTree extends JTree implements ExpandPathListener {

	private static final long serialVersionUID = 5834607662460482028L;

	public IndexTree(TreeModel model) {
		super(model);
		setCellRenderer(new IndexTreeRenderer());
	}

	@Override
	public void expand(ArrayList<Node> path) {
		TreePath treePath = new TreePath(path.toArray());
		setSelectionPath(treePath);
	}

}
