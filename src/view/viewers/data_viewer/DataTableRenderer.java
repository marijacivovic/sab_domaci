package view.viewers.data_viewer;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class DataTableRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = -2370970090401012631L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		Component superResult = super.getTableCellRendererComponent(table,
				value, isSelected, hasFocus, row, column);
		return superResult;
	}

}
