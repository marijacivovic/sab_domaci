package view.viewers.data_viewer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import model.data_files.Field;

public class DataRecordDialog extends JDialog {

	private static final long serialVersionUID = -8125101555511152281L;

	public static final int ADD_MODE = 0;
	public static final int FIND_MODE = 1;
	public static final int SEQ_EDIT_MODE = 5;
	public static final int PK_FIND_MODE = 6;

	private HashMap<String, JTextField> inputFields = new HashMap<String, JTextField>();

	private ArrayList<String> resultRecord;

	public DataRecordDialog(String title, final Field[] fields,
			final String[] initialValues, int mode) {
		super();
		super.setTitle(title);

		setModal(true);

		setLayout(new BorderLayout(4, 4));

		int gridHeight = fields.length;
		if (mode == PK_FIND_MODE) {
			gridHeight = 0;
			for (Field f : fields) {
				if (f.getIsPrimary()) {
					gridHeight++;
				}
			}
		}

		JPanel fieldTitlesPanel = new JPanel();
		fieldTitlesPanel.setLayout(new GridLayout(gridHeight, 1, 4, 4));

		JPanel fieldInputsPanel = new JPanel();
		fieldInputsPanel.setLayout(new GridLayout(gridHeight, 1, 4, 4));

		for (int i = 0; i < fields.length; i++) {
			final Field field = fields[i];

			JTextField inputField = new JTextField(field.getSize());

			if (field.getIsPrimary()) {
				inputField.setBackground(Color.GRAY);
				inputField.setForeground(Color.WHITE);
			}

			inputField.setDocument(new JTextFieldLimit(field.getSize()));

			if (mode == SEQ_EDIT_MODE) {
				if (field.getIsPrimary()) {
					inputField.setEditable(false);
				}
				inputField.setText(initialValues[i].trim());
			}

			inputFields.put(field.getName(), inputField);

			if (field.getIsPrimary() || mode != PK_FIND_MODE) {
				fieldTitlesPanel.add(new JLabel(field.getName(), JLabel.RIGHT));
				JPanel inputFieldWrapper = new JPanel();
				inputFieldWrapper.setLayout(new FlowLayout(FlowLayout.LEADING));
				inputFieldWrapper.add(inputField);
				fieldInputsPanel.add(inputFieldWrapper);
			}
		}

		add(fieldTitlesPanel, BorderLayout.WEST);
		add(fieldInputsPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

		JButton btnOK = new JButton("Ok");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resultRecord = new ArrayList<String>();
				for (int i = 0; i < fields.length; i++) {
					resultRecord.add(getFieldValue(fields[i].getName()));
				}
				setVisible(false);
			}
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resultRecord = null;
				setVisible(false);
			}
		});

		JPanel tempPanel = new JPanel();
		tempPanel.setLayout(new GridLayout(1, 2));
		tempPanel.add(btnOK);
		tempPanel.add(btnCancel);
		buttonsPanel.add(tempPanel);
		add(buttonsPanel, BorderLayout.SOUTH);

		pack();
		setLocationRelativeTo(null);
	}

	private String getFieldValue(String fieldName) {
		String fieldValue = inputFields.get(fieldName).getText();

		if (fieldValue.length() > inputFields.get(fieldName).getColumns())
			return fieldValue.substring(0, inputFields.get(fieldName)
					.getColumns());

		for (int i = inputFields.get(fieldName).getText().length(); i < inputFields
				.get(fieldName).getColumns(); i++)
			fieldValue = fieldValue + ' ';
		return fieldValue;
	}

	public ArrayList<String> getResultRecord() {
		return resultRecord;
	}

	public class JTextFieldLimit extends PlainDocument {

		private static final long serialVersionUID = 7465443816740364491L;

		private int limit;

		JTextFieldLimit(int limit) {
			super();
			this.limit = limit;
		}

		@Override
		public void insertString(int offset, String str, AttributeSet attr)
				throws BadLocationException {
			if (getLength() < limit) {
				int newLength = getLength() + str.length();
				if (newLength > limit) {
					str = str.substring(0, limit - getLength());
				}
				super.insertString(offset, str, attr);
			}
		}
	}

}
