package view.viewers.data_viewer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import observer.EventArgs;
import observer.Observable;
import observer.Observer;
import observer.EventArguments.PropertyChangedEventArgs;

import model.data_files.DataFileBase;
import model.data_files.SerialDataFile;

public class DataFileDetailsPanel extends JPanel implements Observer {

	private static final long serialVersionUID = 2519233710590221296L;

	private DataFileBase file;

	private JTextField blockFactorValue;
	private JButton changeBlockFactorValue;
	private JTextField fileSizeField;
	private JTextField recordSizeField;
	private JTextField recordCountField;
	private JTextField fileAccesses;

	public DataFileDetailsPanel(DataFileBase file) {
		this.file = file;

		JLabel label1 = new JLabel("Block factor: ");
		blockFactorValue = new JTextField(
				String.valueOf(file.getBLOCK_FACTOR()), 10);
		blockFactorValue.setEditable(false);
		blockFactorValue.setFocusable(false);
		changeBlockFactorValue = new JButton("Change");
		changeBlockFactorValue.addActionListener(listener);

		setLayout(new FlowLayout(FlowLayout.CENTER));

		add(label1);
		add(blockFactorValue);
		add(changeBlockFactorValue);

		JLabel label2 = new JLabel("File size:");
		fileSizeField = new JTextField(String.valueOf(file.getFILE_SIZE()), 10);
		fileSizeField.setEditable(false);
		fileSizeField.setFocusable(false);

		add(label2);
		add(fileSizeField);

		JLabel label3 = new JLabel("Record size:");
		recordSizeField = new JTextField(String.valueOf(file.getFileHeader()
				.getRecordSize()), 10);
		recordSizeField.setEditable(false);
		recordSizeField.setFocusable(false);

		add(label3);
		add(recordSizeField);

		JLabel label5 = new JLabel("Record count:");
		recordCountField = new JTextField(String.valueOf(file.getRECORD_NUM()),
				10);
		recordCountField.setEditable(false);
		recordCountField.setFocusable(false);

		add(label5);
		add(recordCountField);

		if (file instanceof SerialDataFile) {
			JLabel label4 = new JLabel("Search accesses:");
			fileAccesses = new JTextField("0", 10);
			fileAccesses.setEditable(false);
			fileAccesses.setFocusable(false);

			add(label4);
			add(fileAccesses);
		}

		updateFileDetails();
	}

	private void updateFileDetails() {
		blockFactorValue.setText(String.valueOf(file.getBLOCK_FACTOR()));
		fileSizeField.setText(String.valueOf(file.getFILE_SIZE()));
		recordSizeField.setText(String.valueOf(file.getFileHeader()
				.getRecordSize()));
		recordCountField.setText(String.valueOf(file.getRECORD_NUM()));

		if (file instanceof SerialDataFile && fileAccesses != null) {
			fileAccesses.setText(String.valueOf(((SerialDataFile) file)
					.getSearchAccesses()));
		}
	}

	private void changeBlockFactor(long newValue) {
		file.setBLOCK_FACTOR(newValue);
	}

	private void changeBlockFactorValueClick() {
		boolean success = false;

		while (!success) {
			Object answer = JOptionPane.showInputDialog(null,
					"Enter new value", "Change block factor",
					JOptionPane.QUESTION_MESSAGE, null, null,
					blockFactorValue.getText());

			if (answer != null) {
				String s = answer.toString();

				long value = -1;
				try {
					value = Long.parseLong(s);

					if (value > 0) {
						changeBlockFactor(value);
						success = true;
					} else {
						JOptionPane.showMessageDialog(null,
								"New value must be positive integer less than "
										+ Long.MAX_VALUE, "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null,
							"New value must be positive integer less than "
									+ Long.MAX_VALUE, "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				success = true;
			}
		}
	}

	private ActionListener listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(changeBlockFactorValue)) {
				changeBlockFactorValueClick();
			}
		}
	};

	@Override
	public void notifyObserver(Observable source, EventArgs args) {
		if (args == null) {
			return;
		}

		if (args instanceof PropertyChangedEventArgs) {
			updateFileDetails();
		}
	}

}
