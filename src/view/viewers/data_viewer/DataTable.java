package view.viewers.data_viewer;

import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;

public class DataTable extends JTable implements
		SelectedItemChangedEventListener<Integer> {

	private static final long serialVersionUID = -1541835571285586996L;

	public DataTable() {
		super();
		setDefaultRenderer(Object.class, new DataTableRenderer());
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setShowGrid(true);
		getSelectionModel().addListSelectionListener(listener);
		getTableHeader().setReorderingAllowed(false);
	}

	private ListSelectionListener listener = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (e.getValueIsAdjusting()) {
				return;
			}

			notifySelectedItemChangedEventListeners(new SelectedItemChangedEvent<Integer>(
					getSelectedRow()));
		}
	};

	private ArrayList<SelectedItemChangedEventListener<Integer>> selectedItemChangedEventListeners = new ArrayList<SelectedItemChangedEventListener<Integer>>();

	public void addSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<Integer> listener) {
		selectedItemChangedEventListeners.add(listener);
	}

	public void removeSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<Integer> listener) {
		selectedItemChangedEventListeners.remove(listener);
	}

	private void notifySelectedItemChangedEventListeners(
			SelectedItemChangedEvent<Integer> args) {
		for (SelectedItemChangedEventListener<Integer> listener : selectedItemChangedEventListeners) {
			listener.selectedItemChanged(this, args);
		}
	}

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<Integer> e) {

		if (e != null) {
			final int row = e.getSelectedItem();
			if (!EventQueue.isDispatchThread()) {
				try {
					EventQueue.invokeAndWait(new Runnable() {
						@Override
						public void run() {
							getSelectionModel().setSelectionInterval(row, row);
						}
					});
				} catch (Exception e1) {
				}
			}
			try {
				Thread.sleep(20);
			} catch (InterruptedException e1) {
			}
		}

	}

}
