package view.viewers.data_viewer;

import gui.MainWindow;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.data_files.DataFileBase;
import model.data_files.IndexDataFile;
import model.data_files.SequenceDataFile;
import model.data_files.SerialDataFile;
import model.filesystemmodel.file_types.FileModel;
import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;
import actions.RefreshFileListAction;

public class ControlButtonsPanel extends JPanel implements
		SelectedItemChangedEventListener<Integer> {

	private static final long serialVersionUID = 6485886834471421468L;

	private DataFileBase file;
	private DataTable dataTable;

	private JButton fetchNextBlockButton;
	private JButton addRecordButton;
	private JButton deleteRecordButton;
	private JButton editRecordButton;
	private JButton findRecordButton;
	private JButton findNextRecordButton;
	private JButton findAllRecordsButton;

	private boolean isSearchResultMode = false;

	public ControlButtonsPanel(DataFileBase file, DataTable dataTable) {
		this.file = file;
		this.dataTable = dataTable;

		setLayout(new FlowLayout(FlowLayout.RIGHT));

		fetchNextBlockButton = new JButton("Fetch Next Block");
		addRecordButton = new JButton("Add Record");
		if (file instanceof SequenceDataFile || file instanceof IndexDataFile) {
			addRecordButton.setEnabled(false);
		}
		deleteRecordButton = new JButton("Delete Record");
		if (file instanceof IndexDataFile) {
			deleteRecordButton.setEnabled(false);
		}
		if (file instanceof SequenceDataFile) {
			editRecordButton = new JButton("Edit record");
			editRecordButton.setEnabled(false);
		}
		findRecordButton = new JButton("Find Record");
		if (file instanceof SerialDataFile) {
			findNextRecordButton = new JButton("Find Next Record");
			findNextRecordButton.setEnabled(false);

			findAllRecordsButton = new JButton("Find All Records");
		}

		deleteRecordButton.setEnabled(false);

		fetchNextBlockButton.addActionListener(listener);
		addRecordButton.addActionListener(listener);
		deleteRecordButton.addActionListener(listener);
		if (editRecordButton != null) {
			editRecordButton.addActionListener(listener);
		}
		findRecordButton.addActionListener(listener);
		if (findNextRecordButton != null) {
			findNextRecordButton.addActionListener(listener);
		}
		if (findAllRecordsButton != null) {
			findAllRecordsButton.addActionListener(listener);
		}

		add(fetchNextBlockButton);
		add(addRecordButton);
		add(deleteRecordButton);
		if (editRecordButton != null) {
			add(editRecordButton);
		}
		add(findRecordButton);
		if (findNextRecordButton != null) {
			add(findNextRecordButton);
		}
		if (findAllRecordsButton != null) {
			add(findAllRecordsButton);
		}
	}

	private HashMap<JButton, Boolean> previousStates = null;
	private boolean allDisabled = false;

	private void disableAllButtons() {
		if (previousStates == null) {
			previousStates = new HashMap<JButton, Boolean>();
		}
		previousStates.clear();

		for (Component c : getComponents()) {
			if (c instanceof JButton) {
				JButton button = (JButton) c;
				previousStates.put(button, button.isEnabled());
				button.setEnabled(false);
			}
		}

		allDisabled = true;
	}

	private void enableAllButtons() {
		if (previousStates == null) {
			return;
		}

		for (Entry<JButton, Boolean> entry : previousStates.entrySet()) {
			entry.getKey().setEnabled(entry.getValue());
		}

		allDisabled = false;
	}

	private void fetchNextBlockButtonClick() {
		try {
			isSearchResultMode = false;
			file.fetchNextBlock();
		} catch (Exception ex) {
			System.err.println(ex);
		}
		if (findNextRecordButton != null) {
			findNextRecordButton.setEnabled(false);
		}
	}

	private void addRecordButtonClick() {
		boolean addMore = true;
		while (addMore) {
			try {
				DataRecordDialog addDialog = new DataRecordDialog("Add Record",
						file.getFileHeader().getFields(), null,
						DataRecordDialog.ADD_MODE);
				addDialog.setVisible(true);

				if (addDialog.getResultRecord() != null) {
					try {
						file.addRecord(addDialog.getResultRecord());

						int addMoreResult = JOptionPane.showConfirmDialog(null,
								"Do you want to add more records?",
								"Add Record", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);

						if (addMoreResult != JOptionPane.YES_OPTION) {
							addMore = false;
						}
					} catch (IOException ex) {
						JOptionPane.showMessageDialog(null,
								"Adding record failed", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					addMore = false;
				}

				addDialog.dispose();
			} catch (Exception ex) {
				System.err.println(ex);
			}
		}
		try {
			file.flush();
		} catch (IOException e) {
		}
	}

	private void deleteRecordButtonClick() {
		int result = JOptionPane.showConfirmDialog(null,
				"Do you sure want to delete selected record?", "Delete Record",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

		if (result != JOptionPane.YES_OPTION) {
			return;
		}

		try {
			int selectedRow = dataTable.getSelectedRow();
			if (selectedRow != -1) {
				ArrayList<String> record = new ArrayList<>();
				int columnCount = dataTable.getColumnCount();
				for (int column = 0; column < columnCount; column++) {
					record.add(dataTable.getValueAt(selectedRow, column)
							.toString());
				}
				file.deleteRecord(record);
			}
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	private void editRecordButtonClick() {
		try {
			int selectedRow = dataTable.getSelectedRow();

			if (selectedRow == -1) {
				return;
			}

			String[] record = new String[file.getFileHeader().getFieldCount()];

			for (int i = 0; i < record.length; i++) {
				record[i] = (String) dataTable.getModel().getValueAt(
						selectedRow, i);
			}

			final DataRecordDialog addDialog = new DataRecordDialog(
					"Update Record", file.getFileHeader().getFields(), record,
					DataRecordDialog.SEQ_EDIT_MODE);
			addDialog.setVisible(true);

			if (addDialog.getResultRecord() != null) {

				file.updateRecord(addDialog.getResultRecord());
			}

			addDialog.dispose();
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	private void findRecordButtonClick() {
		try {
			final DataRecordDialog addDialog = new DataRecordDialog(
					"Find Record",
					file.getFileHeader().getFields(),
					null,
					(file instanceof SerialDataFile) ? DataRecordDialog.FIND_MODE
							: DataRecordDialog.PK_FIND_MODE);

			addDialog.setVisible(true);

			if (addDialog.getResultRecord() != null) {
				disableAllButtons();
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						int positions[] = new int[100];
						for (int i = 0; i < positions.length; i++)
							positions[i] = -1;

						if (file instanceof SerialDataFile) {
							((SerialDataFile) file).setIsFindNextSearch(false);
						}

						boolean result = file.findRecord(
								addDialog.getResultRecord(), positions);

						if (MainWindow.getInstance().getIsClosing()) {
							return;
						}

						enableAllButtons();

						if (result) {
							JOptionPane.showMessageDialog(null,
									"Record found!", "Find Record",
									JOptionPane.INFORMATION_MESSAGE);
							if (findNextRecordButton != null) {
								findNextRecordButton.setEnabled(true);
							}
						} else {
							JOptionPane.showMessageDialog(null,
									"Record not found!", "Find Record",
									JOptionPane.INFORMATION_MESSAGE);
							if (findNextRecordButton != null) {
								findNextRecordButton.setEnabled(false);
							}
						}

						selectedItemChanged(null,
								new SelectedItemChangedEvent<Integer>(0));
					}
				});
				t.setDaemon(true);
				t.start();
			}

			addDialog.dispose();
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	private void findNextRecordButtonClick() {
		if (file instanceof SerialDataFile) {
			SerialDataFile serialFile = (SerialDataFile) file;
			serialFile.setIsFindNextSearch(true);

			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					int positions[] = new int[100];
					for (int i = 0; i < positions.length; i++)
						positions[i] = -1;

					boolean result = file.findRecord(null, positions);

					if (MainWindow.getInstance().getIsClosing()) {
						return;
					}

					enableAllButtons();

					if (result) {
						JOptionPane.showMessageDialog(null, "Record found!",
								"Find Record", JOptionPane.INFORMATION_MESSAGE);
						if (findNextRecordButton != null) {
							findNextRecordButton.setEnabled(true);
						}
					} else {
						JOptionPane.showMessageDialog(null,
								"Record not found!", "Find Record",
								JOptionPane.INFORMATION_MESSAGE);
						if (findNextRecordButton != null) {
							findNextRecordButton.setEnabled(false);
						}
					}
					selectedItemChanged(null,
							new SelectedItemChangedEvent<Integer>(0));
				}
			});
			t.setDaemon(true);
			t.start();
		}
	}

	private void findAllRecordsButtonClick() {
		if (file instanceof SerialDataFile) {
			try {
				final DataRecordDialog addDialog = new DataRecordDialog(
						"Find All Records", file.getFileHeader().getFields(),
						null, DataRecordDialog.FIND_MODE);

				addDialog.setVisible(true);

				if (addDialog.getResultRecord() != null) {

					final int res = JOptionPane.showConfirmDialog(this,
							"Do you want to save search result to file?",
							"Find All Records", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);

					disableAllButtons();
					Thread t = new Thread(new Runnable() {
						@Override
						public void run() {
							int positions[] = new int[100];
							for (int i = 0; i < positions.length; i++)
								positions[i] = -1;

							((SerialDataFile) file).setIsFindNextSearch(false);

							File result = null;
							boolean resultB = false;

							if (res == JOptionPane.YES_OPTION) {
								result = ((SerialDataFile) file)
										.findAllRecords(
												addDialog.getResultRecord(),
												positions);
							} else {
								resultB = ((SerialDataFile) file)
										.findAllRecordsInMemory(
												addDialog.getResultRecord(),
												positions);
							}

							if (MainWindow.getInstance().getIsClosing()) {
								return;
							}

							enableAllButtons();

							if (res == JOptionPane.YES_OPTION) {
								if (result != null) {
									(new RefreshFileListAction())
											.actionPerformed(null);

									System.out.println("Opening file: "
											+ result);
									MainWindow.getInstance().getWorkspace()
											.openFile(new FileModel(result));

									JOptionPane.showMessageDialog(null,
											"Search completed!",
											"Find All Records",
											JOptionPane.INFORMATION_MESSAGE);
								} else {
									JOptionPane.showMessageDialog(null,
											"Search failed!",
											"Find All Records",
											JOptionPane.INFORMATION_MESSAGE);
								}
							} else {
								if (resultB == true) {
									JOptionPane.showMessageDialog(null,
											"Search completed!",
											"Find All Records",
											JOptionPane.INFORMATION_MESSAGE);
								} else {
									JOptionPane.showMessageDialog(null,
											"Search failed!",
											"Find All Records",
											JOptionPane.INFORMATION_MESSAGE);
								}
								if (deleteRecordButton != null) {
									isSearchResultMode = true;
									deleteRecordButton.setEnabled(false);
								}
							}

							selectedItemChanged(null,
									new SelectedItemChangedEvent<Integer>(0));
						}
					});
					t.setDaemon(true);
					t.start();
				}

				addDialog.dispose();
			} catch (Exception ex) {
				System.err.println(ex);
			}
		}
	}

	private ActionListener listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(fetchNextBlockButton)) {
				fetchNextBlockButtonClick();
			} else if (e.getSource().equals(addRecordButton)) {
				addRecordButtonClick();
			} else if (e.getSource().equals(deleteRecordButton)) {
				deleteRecordButtonClick();
			} else if (e.getSource().equals(editRecordButton)) {
				editRecordButtonClick();
			} else if (e.getSource().equals(findRecordButton)) {
				findRecordButtonClick();
			} else if (e.getSource().equals(findNextRecordButton)) {
				findNextRecordButtonClick();
			} else if (e.getSource().equals(findAllRecordsButton)) {
				findAllRecordsButtonClick();
			}
		}
	};

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<Integer> e) {
		if (e.getSelectedItem() != -1) {
			if (!(file instanceof IndexDataFile) && !allDisabled) {
				deleteRecordButton.setEnabled(true);
			}
			if (editRecordButton != null && !allDisabled) {
				editRecordButton.setEnabled(true);
			}
		} else {
			deleteRecordButton.setEnabled(false);
			if (editRecordButton != null) {
				editRecordButton.setEnabled(false);
			}
		}
		if (isSearchResultMode) {
			if (deleteRecordButton != null) {
				deleteRecordButton.setEnabled(false);
			}
		}

	}

}
