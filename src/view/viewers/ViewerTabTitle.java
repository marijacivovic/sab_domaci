package view.viewers;

import gui.WorkspacePanel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.plaf.basic.BasicButtonUI;

import actions.DeleteFileAction;

import observer.EventArgs;
import observer.Observable;
import observer.Observer;

public class ViewerTabTitle extends JPanel implements Observer {

	private static final long serialVersionUID = 307005579006678745L;

	private ViewerBase<?> view;
	private WorkspacePanel workspace;

	private JLabel titleLabel;

	public ViewerTabTitle(ViewerBase<?> view, WorkspacePanel workspace) {
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));

		this.view = view;
		this.workspace = workspace;

		setOpaque(false);

		titleLabel = new JLabel(view.getDisplayTitle());
		titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

		JButton closeBtn = new TabButton();

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		add(titleLabel);

		gbc.gridx++;
		gbc.weightx = 0;
		add(closeBtn, gbc);

		setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

		JPopupMenu popup = new JPopupMenu();
		popup.add(new DeleteFileAction());

		// setComponentPopupMenu(popup);
	}

	private class TabButton extends JButton implements ActionListener {

		private static final long serialVersionUID = -8188710346366523212L;

		public TabButton() {
			setPreferredSize(new Dimension(14, 14));
			setBorder(BorderFactory.createEtchedBorder());
			setBorderPainted(false);
			setContentAreaFilled(false);
			setFocusable(false);
			setRolloverEnabled(true);
			setUI(new BasicButtonUI());
			setToolTipText("Close this tab");

			addActionListener(this);
		}

		@Override
		public void updateUI() {
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g.create();
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			int xLeft = 4;
			int xRight = getWidth() - xLeft - 1;
			int xTop = 4;
			int xBottom = getHeight() - xTop - 1;

			if (getModel().isPressed()) {
				g2.setColor(Color.GRAY);
				g2.setColor(Color.RED.darker());
				g2.fillRoundRect(0, 0, getWidth(), getHeight(),
						getHeight() * 2 / 3, getHeight() * 2 / 3);

				g2.setColor(Color.WHITE);
				g2.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND,
						BasicStroke.JOIN_ROUND));
				g2.drawLine(xLeft, xTop, xRight, xBottom);
				g2.drawLine(xLeft, xBottom, xRight, xTop);
			} else if (getModel().isRollover()) {
				g2.setColor(Color.GRAY.brighter());
				g2.fillRoundRect(0, 0, getWidth(), getHeight(),
						getHeight() * 2 / 3, getHeight() * 2 / 3);

				g2.setColor(Color.GRAY);
				g2.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND,
						BasicStroke.JOIN_ROUND));
				g2.drawLine(xLeft, xTop, xRight, xBottom);
				g2.drawLine(xLeft, xBottom, xRight, xTop);
			} else {
				g2.setColor(Color.BLACK);
				g2.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND,
						BasicStroke.JOIN_ROUND));
				g2.drawLine(xLeft, xTop, xRight, xBottom);
				g2.drawLine(xLeft, xBottom, xRight, xTop);
			}

			g2.dispose();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			workspace.closeView(view);
		}
	}

	private void updateTitleLabel() {
		if (view.canSave()) {
			titleLabel.setText("* " + view.getDisplayTitle());
		} else {
			titleLabel.setText(view.getDisplayTitle());
		}
	}

	@Override
	public void notifyObserver(Observable source, EventArgs args) {
		updateTitleLabel();
	}

}
