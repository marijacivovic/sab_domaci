package view.viewers;

import gui.MainWindow;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import model.FileArtifact;
import model.FileListModel;
import model.SourceArtifact;
import observer.EventArguments.StateChangeEventArgs;

public class TextView extends ViewerBase<FileArtifact> implements
		DocumentListener {

	private static final long serialVersionUID = 8433704703488098371L;

	private JScrollPane scrollPane;
	private JTextArea textArea;

	private boolean isLoaded;

	public TextView(FileArtifact file) {
		super(file);
		setLayout(new BorderLayout());
		textArea = new JTextArea();
		textArea.setFont(new Font("", Font.PLAIN, 12));
		textArea.getDocument().addDocumentListener(this);
		scrollPane = new JScrollPane(textArea);
		add(scrollPane, BorderLayout.CENTER);
		isLoaded = false;
	}

	@Override
	public String getViewerName() {
		return "TextView";
	}

	@Override
	public void load() {
		setCanSave(false);
		isLoaded = false;

		if (getSource() != null) {
			BufferedReader br;
			try {
				br = new BufferedReader(new FileReader(getSource().getFile()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line);
					sb.append("\n");
				}
				br.close();
				textArea.setText(sb.toString());
			} catch (Exception e) {
				e.printStackTrace();
				textArea.setEnabled(false);
			}
		}

		isLoaded = true;
	}

	@Override
	public void changedUpdate(DocumentEvent arg0) {
		System.out.println("Changed update");
	}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		if (!isLoaded) {
			return;
		}
		onChanged();
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		onChanged();
	}

	@Override
	public ViewerBase<?> getClone(SourceArtifact source) {
		if (source instanceof FileArtifact) {
			return new TextView((FileArtifact) source);
		}
		return null;
	}

	private boolean saveToFile(String filePath) {
		try {
			FileWriter writer = new FileWriter(filePath);
			writer.write(textArea.getText());
			writer.close();

			File file = new File(filePath);
			source = new FileArtifact(file);
			
			title = file.getName();
			setCanSave(false);
			notifyObservers(new StateChangeEventArgs("DocumentSaved"));
			return true;
		} catch (IOException e) {
			System.err.println(e);
			return false;
		}
	}

	@Override
	public boolean save() {
		if (getSource() == null) {
			JFileChooser jfc = new JFileChooser(
					((FileListModel) MainWindow.getInstance().getSidebar()
							.getFileList().getModel()).getCurrentFolder());
			int saveResult = jfc.showSaveDialog(MainWindow.getInstance());

			if (saveResult == JFileChooser.APPROVE_OPTION) {
				boolean result = saveToFile(jfc.getSelectedFile()
						.getAbsolutePath());
				return result;
			} else {
				return false;
			}
		} else {
			return saveToFile(source.getFile().getAbsolutePath());
		}
	}

	@Override
	public boolean tryClose() {
		return true;
	}

}
