package view.filelist;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import model.filesystemmodel.file_types.FileSystemNode;

public class FileListCellRenderer extends DefaultListCellRenderer {

	private static final long serialVersionUID = 8593619239872443728L;

	public FileListCellRenderer() {
		super();
	}

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {

		Component superResult = super.getListCellRendererComponent(list, value,
				index, isSelected, cellHasFocus);

		if (value instanceof FileSystemNode) {
			FileSystemNode listItem = (FileSystemNode) value;
			setIcon(listItem.getIcon());
			setText(listItem.getDisplayName());
		}

		return superResult;
	}
}
