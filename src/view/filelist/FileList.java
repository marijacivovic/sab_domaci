package view.filelist;

import gui.MainWindow;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import actions.DeleteFileAction;

import observer.EventArgs;
import observer.Observable;
import observer.Observer;
import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;

import model.FileListModel;
import model.filesystemmodel.file_types.FileModel;
import model.filesystemmodel.file_types.FileSystemNode;

public class FileList extends JList<FileSystemNode> implements Observer,
		ListSelectionListener {

	private static final long serialVersionUID = 7867340426464594613L;

	private FileListModel listModel;

	public FileList() {
		super();

		listModel = new FileListModel(null);
		listModel.addObserver(this);

		setModel(listModel);
		setCellRenderer(new FileListCellRenderer());
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		addMouseListener(new FileListMouseListener());
		addListSelectionListener(this);
	}

	private class FileListMouseListener extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			if (getSelectedIndex() > -1 && arg0.getClickCount() % 2 == 0) {
				FileSystemNode selectedItem = getSelectedValue();
				if (selectedItem.getFile().isDirectory()) {
					listModel.loadFolder(selectedItem.getFile());
				} else {
					boolean result = MainWindow.getInstance().openFile(
							(FileModel) selectedItem);

					if (!result) {
						JOptionPane
								.showMessageDialog(
										null,
										"No viewer associated with this file extension",
										"Error opening file",
										JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			check(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			check(e);
		}

		private void check(MouseEvent e) {
			if (e.isPopupTrigger()) {
				setSelectedIndex(locationToIndex(e.getPoint()));
				if (getSelectedIndex() != -1
						&& getSelectedValue().canBeDeleted()) {
					JPopupMenu popup = new JPopupMenu();
					popup.add(new DeleteFileAction());
					popup.show(FileList.this, e.getX(), e.getY());
				}
			}
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		if (!arg0.getValueIsAdjusting() && this.getSelectedIndex() > -1) {
			notifySelectedItemChangedEventListeners();
		}
	}

	@Override
	public void notifyObserver(Observable o, EventArgs eventArgs) {
		setModel(listModel);
		notifySelectedItemChangedEventListeners();
	}

	private ArrayList<SelectedItemChangedEventListener<FileSystemNode>> selectedItemChangedEventListeners = new ArrayList<SelectedItemChangedEventListener<FileSystemNode>>();

	public void addSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<FileSystemNode> listener) {
		selectedItemChangedEventListeners.add(listener);
	}

	public void removeSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<FileSystemNode> listener) {
		selectedItemChangedEventListeners.remove(listener);
	}

	private void notifySelectedItemChangedEventListeners() {
		for (SelectedItemChangedEventListener<FileSystemNode> listener : selectedItemChangedEventListeners) {
			listener.selectedItemChanged(this,
					new SelectedItemChangedEvent<FileSystemNode>(
							getSelectedValue()));
		}
	}

}
