package view.dbTree;

import gui.MainWindow;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import model.db.DBModel;
import model.db.DBTable;

public class DBTree extends JTree {

	private static final long serialVersionUID = 3475929363252549284L;

	private DBModel dbModel;

	public DBTree() {
		super(new DBModel());
		dbModel = (DBModel) getModel();
		setCellRenderer(new DBTreeRenderer());

		getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);
		setToggleClickCount(3);
		addMouseListener(mouseAdapter);
	}

	public DBModel getDBModel() {
		return dbModel;
	}

	private MouseAdapter mouseAdapter = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			TreePath selectedPath = getSelectionPath();

			if (selectedPath == null) {
				return;
			}

			Object selectedObject = selectedPath.getLastPathComponent();

			if (selectedObject instanceof DBTable) {
				DBTable selectedTable = (DBTable) selectedObject;

				if (e.getClickCount() == 2) {
					MainWindow.getInstance().openTable(selectedTable);
				}
			}
		}
	};

}
