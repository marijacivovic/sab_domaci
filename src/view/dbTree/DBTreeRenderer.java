package view.dbTree;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import model.db.DBNode;

public class DBTreeRenderer extends DefaultTreeCellRenderer {

	private static final long serialVersionUID = -1600940397289504910L;

	public DBTreeRenderer() {

	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {

		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
				row, hasFocus);

		if (value instanceof DBNode) {
			DBNode node = (DBNode) value;
			setText(node.getTitle());
			setIcon(node.getIcon());
		}

		return this;
	}
}
