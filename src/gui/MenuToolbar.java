package gui;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

import actions.ChangeViewerAction;
import actions.CloseAllViewersAction;
import actions.CloseViewerAction;
import actions.ConnectDatabaseAction;
import actions.DeleteFileAction;
import actions.ExitAction;
import actions.HelpAction;
import actions.NewFileAction;
import actions.RefreshFileListAction;
import actions.SaveAction;

public class MenuToolbar extends JMenuBar {

	private static final long serialVersionUID = 5455027111899853611L;

	private NewFileAction newFileAction;
	private SaveAction saveAction;
	private DeleteFileAction deleteAction;
	private RefreshFileListAction refreshAction;
	private ExitAction exitAction;
	private ConnectDatabaseAction connectDatabaseAction;
	private CloseViewerAction closeViewerAction;
	private CloseAllViewersAction closeAllViewersAction;
	private ChangeViewerAction nextViewerAction;
	private ChangeViewerAction previousViewerAction;
	private HelpAction helpAction;

	public MenuToolbar() {
		newFileAction = new NewFileAction("TXT");
		saveAction = new SaveAction();
		deleteAction = new DeleteFileAction();
		refreshAction = new RefreshFileListAction();
		exitAction = new ExitAction();
		connectDatabaseAction = new ConnectDatabaseAction();
		closeViewerAction = new CloseViewerAction();
		closeAllViewersAction = new CloseAllViewersAction();
		nextViewerAction = new ChangeViewerAction(true);
		previousViewerAction = new ChangeViewerAction(false);
		helpAction = new HelpAction();

		JMenu file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);
		file.add(newFileAction);
		file.add(saveAction);
		file.addSeparator();
		file.add(deleteAction);
		file.add(refreshAction);
		file.addSeparator();
		file.add(exitAction);

		JMenu connect = new JMenu("Connect");
		connect.setMnemonic(KeyEvent.VK_C);
		connect.add(connectDatabaseAction);

		JMenu window = new JMenu("Window");
		window.setMnemonic(KeyEvent.VK_W);
		window.add(closeViewerAction);
		window.add(closeAllViewersAction);
		window.addSeparator();
		window.add(nextViewerAction);
		window.add(previousViewerAction);

		JMenu help = new JMenu("Help");
		help.setMnemonic(KeyEvent.VK_H);
		help.add(helpAction);

		add(file);
		add(connect);
		add(window);
		add(help);
	}

	public NewFileAction getNewFileAction() {
		return newFileAction;
	}

	public SaveAction getSaveAction() {
		return saveAction;
	}

	public DeleteFileAction getDeleteAction() {
		return deleteAction;
	}

	public RefreshFileListAction getRefreshAction() {
		return refreshAction;
	}

	public ExitAction getExitAction() {
		return exitAction;
	}

	public ConnectDatabaseAction getConnectDatabaseAction() {
		return connectDatabaseAction;
	}

	public CloseViewerAction getCloseViewerAction() {
		return closeViewerAction;
	}

	public CloseAllViewersAction getCloseAllViewersAction() {
		return closeAllViewersAction;
	}

	public ChangeViewerAction getNextViewerAction() {
		return nextViewerAction;
	}

	public ChangeViewerAction getPreviousViewerAction() {
		return previousViewerAction;
	}

	public HelpAction getHelpAction() {
		return helpAction;
	}

}
