package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;

import resources.ResourceLoader;
import view.viewers.ViewerBase;

import action_buttons.ActionButton;

import model.db.DBTable;
import model.filesystemmodel.file_types.FileModel;

public class MainWindow extends JFrame implements
		SelectedItemChangedEventListener<ViewerBase<?>> {

	private static final long serialVersionUID = 2740437090361841747L;

	private static final String appTitle = "Project";

	private static MainWindow instance;

	public static MainWindow getInstance() {
		if (instance == null) {
			instance = new MainWindow();
			instance.initialize();
		}
		return instance;
	}

	private MenuToolbar menu;
	private Toolbar toolbar;
	private SidebarPanel sidebar;
	private WorkspacePanel workspace;
	private DetailsPanel detailsPanel;

	private volatile boolean isClosing;

	private MainWindow() {

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});

	}

	private void initialize() {
		setTitle(appTitle);
		setSize(800, 600);
		setIconImage(ResourceLoader.loadImage("application.png"));
		setMinimumSize(new Dimension(320, 280));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());

		initializeMenuBarAndToolbar();

		sidebar = new SidebarPanel();
		workspace = new WorkspacePanel();
		detailsPanel = new DetailsPanel();

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				sidebar, workspace);
		splitPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		splitPane.setDividerLocation(250);

		add(splitPane, BorderLayout.CENTER);
		add(detailsPanel, BorderLayout.SOUTH);

		sidebar.getFileList().addSelectedItemChangedEventListener(detailsPanel);
		sidebar.getFileList().addSelectedItemChangedEventListener(
				menu.getDeleteAction());

		workspace.addSelectedItemChangedEventListener(this);
		workspace.addSelectedItemChangedEventListener(menu
				.getCloseViewerAction());
		workspace.addSelectedItemChangedEventListener(menu
				.getCloseAllViewersAction());
		workspace.addSelectedItemChangedEventListener(menu
				.getNextViewerAction());
		workspace.addSelectedItemChangedEventListener(menu
				.getPreviousViewerAction());

		toolbar.getDriveButtonsPanel().selectInitialDrive();

		try {
			for (LookAndFeelInfo look : UIManager.getInstalledLookAndFeels()) {
				if (look.getName().equals("Nimbus")) {
					UIManager.setLookAndFeel(look.getClassName());
				}
			}
		} catch (Exception ex) {
			System.err.println(ex);
		}

		SwingUtilities.updateComponentTreeUI(this);
	}

	private void initializeMenuBarAndToolbar() {
		menu = new MenuToolbar();
		setJMenuBar(menu);

		toolbar = new Toolbar();
		toolbar.getToolbarButtons().add(
				new ActionButton(menu.getNewFileAction()));
		toolbar.getToolbarButtons().add(new ActionButton(menu.getSaveAction()));
		toolbar.getToolbarButtons().addSeparator();
		toolbar.getToolbarButtons().add(
				new ActionButton(menu.getDeleteAction()));
		toolbar.getToolbarButtons().add(
				new ActionButton(menu.getRefreshAction()));
		toolbar.getToolbarButtons().addSeparator();
		toolbar.getToolbarButtons().add(
				new ActionButton(menu.getConnectDatabaseAction()));
		toolbar.getToolbarButtons().addSeparator();
		toolbar.getToolbarButtons().add(new ActionButton(menu.getExitAction()));
		add(toolbar, BorderLayout.NORTH);
	}

	public JMenuBar getMenu() {
		return menu;
	}

	public Toolbar getToolbar() {
		return toolbar;
	}

	public SidebarPanel getSidebar() {
		return sidebar;
	}

	public WorkspacePanel getWorkspace() {
		return workspace;
	}

	public DetailsPanel getDetailsPanel() {
		return detailsPanel;
	}

	public boolean openFile(FileModel file) {
		return workspace.openFile(file);
	}

	public boolean openTable(DBTable table) {
		return workspace.openTable(table);
	}

	public void close() {
		if (workspace.closeAllViews()) {
			isClosing = true;
			setVisible(false);
			dispose();
		} else {
			System.out.println("Failed to close all views");
		}
	}

	public boolean getIsClosing() {
		return isClosing;
	}

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<ViewerBase<?>> e) {
		menu.getSaveAction().setSaveable(e.getSelectedItem());
		if (e.getSelectedItem() == null) {
			setTitle(appTitle);
		} else {
			setTitle(e.getSelectedItem().getDisplayTitle() + " - " + appTitle);
		}
	}

}
