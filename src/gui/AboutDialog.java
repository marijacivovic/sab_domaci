package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import resources.ResourceLoader;

public class AboutDialog extends JDialog {

	private static final long serialVersionUID = -4147155878851661385L;

	public AboutDialog() {
		setTitle("About UI Projekat");
		setSize(370, 160);
		setResizable(false);
		setModal(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		JPanel imagePanel = new JPanel();
		
		JLabel imageIcon = new JLabel();
		imageIcon.setIcon(new ImageIcon(ResourceLoader.loadImage("author.jpg")));
		imagePanel.add(imageIcon, BorderLayout.WEST);
		imageIcon.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
		
		Font keyFont = new Font("Dialog", Font.PLAIN, 14);
		Font valueFont = new Font("Dialog", Font.BOLD, 14);
		
		JPanel details = new JPanel();
		details.setLayout(new GridLayout(5, 1));
		
		JPanel nameRow = new JPanel();
		JLabel label1 = new JLabel("Name:");
		label1.setFont(keyFont);
		JLabel nameLabel = new JLabel("SmiljkoviŠ Nikola");
		nameLabel.setFont(valueFont);
		nameRow.add(label1);
		nameRow.add(nameLabel);

		JPanel indexNumberRow = new JPanel();
		JLabel label2 = new JLabel("Index:");
		label2.setFont(keyFont);
		JLabel indexLabel = new JLabel("RN-09/11");
		indexLabel.setFont(valueFont);
		indexNumberRow.add(label2);
		indexNumberRow.add(indexLabel);
		
		JPanel mailRow = new JPanel();
		JLabel label3 = new JLabel("E-mail:");
		label3.setFont(keyFont);
		JLabel mailLabel = new JLabel("nsmiljkovic11@raf.edu.rs");
		mailLabel.setFont(valueFont);
		mailRow.add(label3);
		mailRow.add(mailLabel);
		
		details.add(new JPanel());
		details.add(nameRow);
		details.add(indexNumberRow);
		details.add(mailRow);
		details.add(new JPanel());
		
		add(imagePanel, BorderLayout.WEST);
		add(details, BorderLayout.EAST);
	}

}
