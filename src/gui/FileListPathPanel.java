package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;

import observer.EventArgs;
import observer.Observable;
import observer.Observer;
import observer.EventArguments.PropertyChangedEventArgs;

public class FileListPathPanel extends JPanel implements Observer {

	private static final long serialVersionUID = -5052813857690505404L;

	private JTextField pathLabel;

	public FileListPathPanel() {
		setPreferredSize(new Dimension(50, 20));
		setLayout(new GridLayout(1, 1));
		pathLabel = new JTextField();
		pathLabel.setEditable(false);
		pathLabel.setEnabled(false);
		pathLabel.setDisabledTextColor(Color.BLACK);
		pathLabel.setBackground(getBackground());
		pathLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		add(pathLabel);
	}

	private void changePathLabel(String pathText) {
		pathLabel.setText(pathText);
	}

	@Override
	public void notifyObserver(Observable source, EventArgs args) {
		if (args instanceof PropertyChangedEventArgs) {
			PropertyChangedEventArgs pcEventArgs = (PropertyChangedEventArgs) args;
			if (((PropertyChangedEventArgs) args).getPropertyName().equals(
					"CurrentFolder")) {
				changePathLabel(((File) pcEventArgs.getNewValue()).getAbsolutePath());
			}
		}
	}
}
