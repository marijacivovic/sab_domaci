package gui;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.FileArtifact;
import model.db.DBTable;
import model.filesystemmodel.file_types.FileModel;
import observer.EventArgs;
import observer.Observable;
import observer.Observer;
import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;
import observer.EventArguments.StateChangeEventArgs;
import resources.ResourceLoader;
import util.Util;
import view.viewers.ImageView;
import view.viewers.TextView;
import view.viewers.ViewerBase;
import view.viewers.ViewerTabTitle;
import view.viewers.data_viewer.DataView;
import view.viewers.db_table_viewer.DBTableViewer;

public class WorkspacePanel extends JTabbedPane implements Observer {

	private static final long serialVersionUID = 2866443136666214923L;

	private ArrayList<ViewerBase<?>> viewersList;

	private ArrayList<ViewerBase<?>> allViews;

	public WorkspacePanel() {
		allViews = new ArrayList<ViewerBase<?>>();
		setFocusable(false);
		initializeViewersList();

		addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				notifySeletedItemChangedEventListeners();
			}
		});
	}

	private void initializeViewersList() {
		viewersList = new ArrayList<ViewerBase<?>>();
		viewersList.add(new TextView(null));
		viewersList.add(new ImageView(null));
		viewersList.add(new DataView(null));
	}

	public void showNextTab() {
		int currentIndex = getSelectedIndex();
		if (currentIndex != -1) {
			int nextTabIndex = (currentIndex + 1) % allViews.size();
			setSelectedIndex(nextTabIndex);
		}
	}

	public void showPreviousTab() {
		int currentIndex = getSelectedIndex();
		if (currentIndex != -1) {
			int previousTabIndex = (currentIndex - 1 + allViews.size())
					% allViews.size();
			setSelectedIndex(previousTabIndex);
		}
	}

	private void addView(ViewerBase<?> view) {
		addTab(view.getDisplayTitle(), view);
		int index = indexOfComponent(view);

		ViewerTabTitle tabTitle = new ViewerTabTitle(view, this);
		view.addObserver(tabTitle);
		setTabComponentAt(index, tabTitle);

		view.addObserver(this);
		view.load();
		allViews.add(view);
		setSelectedComponent(view);
	}

	public boolean openFile(FileModel file) {
		for (ViewerBase<?> view : allViews) {
			if (view.getSource() != null
					&& view.getSource()
							.equals(new FileArtifact(file.getFile()))) {
				setSelectedComponent(view);
				return true;
			}
		}

		for (ViewerBase<?> view : viewersList) {
			if (view.getViewerName().equals(
					ResourceLoader.getViewAssociationMap().getValue(
							Util.getFileExtension(file.getFile())))) {

				ViewerBase<?> newView = view.getClone(new FileArtifact(file
						.getFile()));
				addView(newView);
				return true;
			}
		}

		return false;
	}

	public boolean openTable(DBTable table) {
		for (ViewerBase<?> view : allViews) {
			if (view.getSource() != null && view.getSource().equals(table)) {
				setSelectedComponent(view);
				return true;
			}
		}

		ViewerBase<?> newView = new DBTableViewer(table);
		addView(newView);

		return true;
	}
	
	public void closeAllTables() {
		ArrayList<ViewerBase<?>> openedTables = new ArrayList<>();
		for (ViewerBase<?> viewer : allViews) {
			if (viewer instanceof DBTableViewer) {
				openedTables.add(viewer);
			}
		}
		for (ViewerBase<?> viewer : openedTables) {
			closeView(viewer);
		}
	}

	public boolean createNewFile(String fileExtension) {
		for (ViewerBase<?> view : viewersList) {
			if (view.getViewerName().equals(
					ResourceLoader.getViewAssociationMap().getValue(
							"." + fileExtension))) {
				ViewerBase<?> newView = view.getClone(null);
				if (newView != null) {
					addView(newView);
					return true;
				} else {
					return false;
				}
			}
		}
		return true;
	}

	public boolean closeView(ViewerBase<?> view) {
		int viewerIndex = indexOfComponent(view);

		if (viewerIndex == -1) {
			return true;
		}

		if (!view.close()) {
			return false;
		}

		remove(view);

		view.removeObserver(this);
		allViews.remove(view);

		return true;
	}

	public boolean closeAllViews() {
		while (allViews.size() > 0) {
			if (!closeView(allViews.get(0))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void notifyObserver(Observable source, EventArgs args) {
		if (args instanceof StateChangeEventArgs) {
			StateChangeEventArgs scEventArgs = (StateChangeEventArgs) args;
			if (scEventArgs.getState().equals("DocumentSaved")) {
				((MenuToolbar) (MainWindow.getInstance().getJMenuBar()))
						.getRefreshAction().actionPerformed(
								new ActionEvent(this, 0, ""));
			}
		}
	}

	private ArrayList<SelectedItemChangedEventListener<ViewerBase<?>>> selectedItemChangeEventListeners = new ArrayList<SelectedItemChangedEventListener<ViewerBase<?>>>();

	public void addSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<ViewerBase<?>> o) {
		selectedItemChangeEventListeners.add(o);
	}

	public void removeSelectedItemChangedEventListener(
			SelectedItemChangedEventListener<ViewerBase<?>> listener) {
		selectedItemChangeEventListeners.remove(listener);
	}

	private void notifySeletedItemChangedEventListeners() {
		SelectedItemChangedEvent<ViewerBase<?>> event = null;
		if (getSelectedComponent() instanceof ViewerBase) {
			event = new SelectedItemChangedEvent<ViewerBase<?>>(
					(ViewerBase<?>) getSelectedComponent());
		} else {
			event = new SelectedItemChangedEvent<ViewerBase<?>>(null);
		}
		for (SelectedItemChangedEventListener<ViewerBase<?>> listener : selectedItemChangeEventListeners) {
			listener.selectedItemChanged(this, event);
		}
	}
}
