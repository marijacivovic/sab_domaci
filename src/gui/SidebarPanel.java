package gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import model.FileListModel;
import view.dbTree.DBTree;
import view.filelist.FileList;

public class SidebarPanel extends JPanel {

	private static final long serialVersionUID = 3865119042621348589L;

	private FileListPathPanel pathPanel;
	private FileList fileList;

	private DBTree dbTree;

	public SidebarPanel() {
		pathPanel = new FileListPathPanel();
		fileList = new FileList();
		dbTree = new DBTree();

		JPanel fileListPanel = new JPanel();
		fileListPanel.setLayout(new BorderLayout());
		fileListPanel
				.setBorder(BorderFactory.createTitledBorder(
						BorderFactory.createLineBorder(Color.GRAY, 2),
						"File Explorer"));

		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));

		JScrollPane scrollPane = new JScrollPane(fileList);
		centerPanel.add(scrollPane, BorderLayout.CENTER);

		fileListPanel.add(pathPanel, BorderLayout.NORTH);
		fileListPanel.add(centerPanel, BorderLayout.CENTER);

		JPanel dbTreePanel = new JPanel(new BorderLayout());
		JScrollPane dbTreeScrollPane = new JScrollPane(dbTree);
		
		dbTreePanel.add(dbTreeScrollPane, BorderLayout.CENTER);
		dbTreePanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(Color.GRAY, 2),
				"Database Explorer"));

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
				fileListPanel, dbTreePanel);
		splitPane.setDividerLocation(200);

		setLayout(new BorderLayout());
		add(splitPane, BorderLayout.CENTER);

		((FileListModel) fileList.getModel()).addObserver(pathPanel);
	}

	public FileListPathPanel getFileListPathPanel() {
		return pathPanel;
	}

	public FileList getFileList() {
		return fileList;
	}

	public DBTree getDBTree() {
		return dbTree;
	}

}
