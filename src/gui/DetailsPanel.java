package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import observer.SelectedItemChangedEvent;
import observer.SelectedItemChangedEventListener;

import model.filesystemmodel.file_types.FileSystemNode;

public class DetailsPanel extends JPanel implements
		SelectedItemChangedEventListener<FileSystemNode> {

	private static final long serialVersionUID = -5976079135028613700L;

	private JPanel labelsPanel;
	private JPanel valuesPanel;

	public DetailsPanel() {
		setPreferredSize(new Dimension(150, 70));
		setLayout(new BorderLayout(15, 0));

		labelsPanel = new JPanel();
		labelsPanel.setLayout(new GridLayout(0, 1));
		add(labelsPanel, BorderLayout.WEST);

		valuesPanel = new JPanel();
		valuesPanel.setLayout(new GridLayout(0, 1));
		add(valuesPanel, BorderLayout.CENTER);
	}

	private void changeFile(FileSystemNode file) {
		labelsPanel.removeAll();
		valuesPanel.removeAll();
		if (file == null) {

		} else {
			ArrayList<SimpleEntry<String, String>> attributes = file
					.getAttributes();

			for (int i = 0; i < attributes.size(); i++) {
				SimpleEntry<String, String> attribute = attributes.get(i);

				JLabel label = new JLabel("  " + attribute.getKey() + ":");
				JLabel value = new JLabel(attribute.getValue());
				value.setFont(new Font(value.getFont().getName(), Font.BOLD, 12));

				labelsPanel.add(label);
				valuesPanel.add(value);
			}
		}
		revalidate();
	}

	@Override
	public void selectedItemChanged(Object source,
			SelectedItemChangedEvent<FileSystemNode> e) {
		changeFile(e.getSelectedItem());
	}

}
