package gui;

import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import actions.ChangeDriveAction;

import resources.ResourceLoader;

public class DriveButtonsPanel extends JToolBar {

	private static final long serialVersionUID = -6604215375641775557L;

	private ArrayList<JToggleButton> buttons;
	private File currentDrive;

	private ChangeDriveAction changeDriveAction;

	public DriveButtonsPanel() {
		setFloatable(false);
		setLayout(new FlowLayout(FlowLayout.CENTER, 2, 0));
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		buttons = new ArrayList<JToggleButton>();
		currentDrive = null;

		for (File f : File.listRoots()) {
			JToggleButton driveButton = new JToggleButton(f.getPath()
					.substring(0, 1), ResourceLoader.loadIcon("drive.png"));

			driveButton.setIconTextGap(1);
			driveButton.setFocusable(false);
			driveButton.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));
			driveButton.setFont(new Font("Dialog", Font.PLAIN, 12));
			driveButton.setActionCommand(f.getAbsolutePath());

			driveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					for (JToggleButton button : buttons) {
						button.setSelected(false);
					}
					((JToggleButton) e.getSource()).setSelected(true);
					if (!e.getActionCommand().equals(
							currentDrive.getAbsolutePath())) {
						currentDrive = new File(e.getActionCommand());
						changeDriveAction.actionPerformed(new ActionEvent(
								DriveButtonsPanel.this, 0, e.getActionCommand()));
					}
				}
			});

			add(driveButton);
			buttons.add(driveButton);
		}

		if (buttons.size() > 0) {
			currentDrive = new File(buttons.get(0).getActionCommand());
		}

		changeDriveAction = new ChangeDriveAction();
	}

	public void selectInitialDrive() {
		for (JToggleButton driveButton : buttons) {
			if (driveButton.getActionCommand().equals(
					currentDrive.getAbsolutePath())) {
				driveButton.setSelected(true);
				changeDriveAction.actionPerformed(new ActionEvent(
						DriveButtonsPanel.this, 0, driveButton.getActionCommand()));
			}
		}
	}

	public File getSelectedDrive() {
		return currentDrive;
	}

}
