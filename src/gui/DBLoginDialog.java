package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import model.db.ConnectionDetails;

public class DBLoginDialog extends JDialog {

	private static final long serialVersionUID = 5524918273211445766L;

	private static String defaultServerName = "localhost";
	private static String defaultDatabaseName = "RAF_UI_TEMP";
	private static String defaultUsername = "sa";
	private static String defaultPassword = "Password123";

	private ConnectionDetails connectionDetails;

	private JLabel usernameLabel;
	private JLabel passwordLabel;

	private JTextField serverNameField;
	private JTextField databaseNameField;
	private JRadioButton windowsRadioButton;
	private JRadioButton sqlRadioButton;
	private JTextField usernameField;
	private JPasswordField passwordField;

	private JButton connectButton;
	private JButton cancelButton;

	public DBLoginDialog() {
		super(MainWindow.getInstance(), "Login Form", true);
		connectionDetails = null;

		setLayout(new BorderLayout());

		createForm();

		createButtons();

		pack();

		setLocationRelativeTo(null);
		setResizable(false);
	}

	private void createForm() {
		JPanel formPanel = new JPanel(new BorderLayout(10, 10));

		JPanel labelsPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldsPanel = new JPanel(new GridLayout(0, 1));

		serverNameField = new JTextField(defaultServerName, 15);
		databaseNameField = new JTextField(defaultDatabaseName, 15);
		usernameField = new JTextField(defaultUsername, 15);
		passwordField = new JPasswordField(defaultPassword, 15);
		
		serverNameField.addFocusListener(focusListener);
		databaseNameField.addFocusListener(focusListener);
		usernameField.addFocusListener(focusListener);
		passwordField.addFocusListener(focusListener);
		

		labelsPanel.add(new JLabel("Server Name:"));
		fieldsPanel.add(serverNameField);

		labelsPanel.add(new JLabel("Database Name:"));
		fieldsPanel.add(databaseNameField);

		labelsPanel.add(new JLabel("Authentication:"));

		windowsRadioButton = new JRadioButton("Windows");
		windowsRadioButton.setEnabled(false);
		sqlRadioButton = new JRadioButton("SQL Server");
		sqlRadioButton.setSelected(true);

		windowsRadioButton.addActionListener(radioListener);
		sqlRadioButton.addActionListener(radioListener);

		ButtonGroup authGroup = new ButtonGroup();
		authGroup.add(windowsRadioButton);
		authGroup.add(sqlRadioButton);

		JPanel authPanel = new JPanel(new GridLayout(1, 2));
		authPanel.add(windowsRadioButton);
		authPanel.add(sqlRadioButton);
		fieldsPanel.add(authPanel);

		usernameLabel = new JLabel("Username:");
		labelsPanel.add(usernameLabel);
		fieldsPanel.add(usernameField);

		passwordLabel = new JLabel("Password:");
		labelsPanel.add(passwordLabel);
		fieldsPanel.add(passwordField);

		formPanel.add(labelsPanel, BorderLayout.WEST);
		formPanel.add(fieldsPanel, BorderLayout.CENTER);

		formPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		add(formPanel, BorderLayout.CENTER);
	}

	private void createButtons() {
		connectButton = new JButton("Connect");
		cancelButton = new JButton("Cancel");

		connectButton.addActionListener(clickListener);
		cancelButton.addActionListener(clickListener);

		JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonsPanel.add(connectButton);
		buttonsPanel.add(cancelButton);

		add(buttonsPanel, BorderLayout.SOUTH);
	}

	public ConnectionDetails getConnectionDetails() {
		return connectionDetails;
	}

	private void connectButtonClick(ActionEvent e) {
		connectionDetails = new ConnectionDetails(serverNameField.getText(),
				databaseNameField.getText(), usernameField.getText(),
				passwordField.getPassword());
		setVisible(false);
	}

	private void cancelButtonClick(ActionEvent e) {
		setVisible(false);
	}

	private void windowsRadioButtonClick() {
		usernameLabel.setEnabled(false);
		usernameField.setEnabled(false);
		passwordLabel.setEnabled(false);
		passwordField.setEnabled(false);
	}

	private void sqlServerRadioButtonClick() {
		usernameLabel.setEnabled(true);
		usernameField.setEnabled(true);
		passwordLabel.setEnabled(true);
		passwordField.setEnabled(true);
	}

	private ActionListener clickListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(connectButton)) {
				connectButtonClick(e);
			} else if (e.getSource().equals(cancelButton)) {
				cancelButtonClick(e);
			}
		}

	};

	private ActionListener radioListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(windowsRadioButton)) {
				windowsRadioButtonClick();
			} else if (e.getSource().equals(sqlRadioButton)) {
				sqlServerRadioButtonClick();
			}
		}
	};

	private FocusAdapter focusListener = new FocusAdapter() {

		@Override
		public void focusGained(FocusEvent e) {
			Component component = e.getComponent();
			if (component instanceof JTextComponent) {
				((JTextField) component).selectAll();
			}
		}
	};

}
