package gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JToolBar;

public class Toolbar extends JPanel {

	private static final long serialVersionUID = -5707394191276063225L;
	
	private JToolBar toolbarButtons;
	private DriveButtonsPanel driveButtons;
	
	public Toolbar() {
		setLayout(new BorderLayout());
		
		toolbarButtons = new JToolBar();
		toolbarButtons.setFloatable(false);
		add(toolbarButtons, BorderLayout.NORTH);
		
		driveButtons = new DriveButtonsPanel();
		add(driveButtons, BorderLayout.WEST);
	}
	
	public JToolBar getToolbarButtons() {
		return toolbarButtons;
	}
	
	public DriveButtonsPanel getDriveButtonsPanel() {
		return driveButtons;
	}
	
}
