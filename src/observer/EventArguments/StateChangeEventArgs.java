package observer.EventArguments;

import observer.EventArgs;

public class StateChangeEventArgs extends EventArgs {

	private String state;
	
	public StateChangeEventArgs(String state) {
		this.state = state;
	}
	
	public String getState() {
		return state;
	}
}
