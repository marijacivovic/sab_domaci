package observer.EventArguments;

import observer.EventArgs;

public class PropertyChangedEventArgs extends EventArgs {

	private String propertyName;
	private Object newValue;
	
	public PropertyChangedEventArgs(String propertyName, Object newValue) {
		this.propertyName = propertyName;
		this.newValue = newValue;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	
	public Object getNewValue() {
		return newValue;
	}
	
}
