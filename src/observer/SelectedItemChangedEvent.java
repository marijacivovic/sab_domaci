package observer;

public class SelectedItemChangedEvent<T> {

	private T selectedItem;

	public SelectedItemChangedEvent(T item) {
		selectedItem = item;
	}

	public T getSelectedItem() {
		return selectedItem;
	}
}
