package observer;

public interface UpdateBlockEventListener {

	void blockUpdated(UpdateBlockEvent e);
	
}
