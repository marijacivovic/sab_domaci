package observer;

public interface SelectedItemChangedEventListener<T> {

	void selectedItemChanged(Object source, SelectedItemChangedEvent<T> e);
	
}
