package observer;

public class UpdateBlockEvent {

	private Object[][] data;
	
	public UpdateBlockEvent(Object[][] data) {
		this.data = data;
	}
	
	public Object[][] getData() {
		return data;
	}
	
}
