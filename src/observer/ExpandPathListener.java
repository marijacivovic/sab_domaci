package observer;

import java.util.ArrayList;

import model.tree.Node;

public interface ExpandPathListener {

	void expand(ArrayList<Node> path);
	
}
