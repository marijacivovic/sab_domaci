package observer;

public interface Observer {
	void notifyObserver(Observable source, EventArgs args);
}
