package observer;

public class EventArgs {

	public EventArgs() {
		
	}
	
	private final static EventArgs emptyEventArg = new EventArgs();
	
	public final static EventArgs empty() {
		return emptyEventArg;
	}
}
