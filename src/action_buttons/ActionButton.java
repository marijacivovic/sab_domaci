package action_buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;

public final class ActionButton extends JButton implements ActionListener,
		PropertyChangeListener {

	private static final long serialVersionUID = -5295707349253129305L;

	private AbstractAction action;

	public ActionButton(AbstractAction action) {
		this.action = action;
		setFocusable(false);
		addActionListener(this);

		updateState();

		if (action != null) {
			action.addPropertyChangeListener(this);
		}
	}

	private void updateState() {
		if (action != null) {
			setIcon((Icon) action.getValue(AbstractAction.SMALL_ICON));
			setToolTipText(action.getValue(AbstractAction.SHORT_DESCRIPTION)
					.toString());
			setEnabled((boolean) action.getValue("enabled"));
		}
	}

	@Override
	public final void actionPerformed(ActionEvent e) {
		if (action != null) {
			action.actionPerformed(new ActionEvent(this, 0, ""));
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateState();
	}

}
